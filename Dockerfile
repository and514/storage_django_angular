# pull official base image
FROM python:3.8.2-alpine3.10

# set work directory
RUN mkdir /usr/src/backend
WORKDIR /usr/src/backend

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

# install dependencies
RUN pip install --upgrade pip
COPY ./backend/requirements.txt /usr/src/backend/requirements.txt
RUN pip install -r /usr/src/backend/requirements.txt

RUN mkdir /usr/src/frontend_static

COPY ./frontend_static/scripts/docker-entrypoint.sh /usr/src/frontend_static/entrypoint.sh
#ENTRYPOINT ["/usr/src/frontend_static/entrypoint.sh" ]
