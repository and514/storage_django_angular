import functools
import os
from contextlib import closing

import psycopg2
from psycopg2.extras import DictCursor


def postgresql_raw(fn):
    @functools.wraps(fn)
    def _inner(*args, **kwargs):
        with closing(psycopg2.connect(
                dbname=os.getenv('POSTGRES_DB'),
                user=os.getenv('POSTGRES_USER'),
                password=os.getenv('POSTGRES_PASSWORD'),
                host=os.getenv('POSTGRES_HOST'),
                port=os.getenv('POSTGRES_PORT'),
        )) as conn:
            with conn.cursor(cursor_factory=DictCursor) as cursor:
                cursor.execute(*fn(*args, **kwargs))
                return cursor.fetchall()

    return _inner
