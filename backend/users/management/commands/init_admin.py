from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        user_model = get_user_model()
        if user_model.objects.count() == 0:
            admin = settings.ADMIN.split(' ')
            if admin and len(admin) == 3:
                username, email, password = admin
                print('Creating account for %s (%s)' % (username, email))
                admin = user_model.objects.create_superuser(email=email, username=username, password=password)
                admin.is_active = True
                admin.is_admin = True
                admin.save()
        else:
            print('Admin accounts can only be initialized if no Accounts exist')
