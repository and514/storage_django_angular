def user_info(user):
    return {
        'auth': user.is_authenticated,
        'user_id': user.id,
        'user_name': user.username,
    }
