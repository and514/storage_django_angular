from django.contrib.auth import authenticate, login, logout
from django.http import Http404
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework import permissions
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from users.utils import user_info


@ensure_csrf_cookie
@api_view(('GET',))
@permission_classes((permissions.AllowAny,))
@renderer_classes((JSONRenderer,))
def set_csrf_cookie(request):
    return Response(
        {'result': user_info(request.user)}, status=status.HTTP_200_OK
    )


@api_view(('POST',))
@permission_classes((permissions.AllowAny,))
@renderer_classes((JSONRenderer,))
def api_login(request):
    username = request.data['username']
    password = request.data['password']
    user = authenticate(request, username=username, password=password)
    if user is not None and user.is_active:
        login(request, user)
    else:
        raise Http404("does not exist")

    return Response(
        {'result': user_info(request.user)}, status=status.HTTP_200_OK
    )


@api_view(('POST',))
@permission_classes((permissions.IsAuthenticated,))
@renderer_classes((JSONRenderer,))
def api_logout(request):
    logout(request)
    return Response({'result': 'ok'}, status=status.HTTP_200_OK)


class WebClientPageNumberPagination(PageNumberPagination):
    page_size_query_param = 'size'
