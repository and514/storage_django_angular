from django.db import models

from core.models import BaseModel
from shop.consts import PRODUCT_AMOUNT_MAX_DIGITS, PRODUCT_AMOUNT_DECIMAL_PLACES
from users.consts import DOCUMENT_NUMBER_MAX_LEN


class Measure(BaseModel):
    name = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return f'[{self.id}] {self.name}'


class Product(BaseModel):
    name = models.CharField(max_length=30)
    measure = models.ForeignKey(Measure, on_delete=models.PROTECT)

    class Meta:
        unique_together = ('name', 'measure')

    def __str__(self):
        return f'[{self.id}] {self.name} {self.measure and self.measure.name or ""}'


class Shop(BaseModel):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return f'[{self.id}] {self.name}'


class DocType(BaseModel):
    name = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return f'[{self.id}] {self.name}'


class Document(BaseModel):
    number = models.CharField(max_length=DOCUMENT_NUMBER_MAX_LEN, unique=True, null=True)
    date = models.DateField()
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    type = models.ForeignKey(DocType, on_delete=models.PROTECT)
    manager = models.ForeignKey('auth.User', on_delete=models.PROTECT)

    @property
    def positions(self):
        return self.doc_details.count()

    def __str__(self):
        return f'[{self.id}] {self.date} {self.shop}'


class DocumentDetails(BaseModel):
    document = models.ForeignKey(Document, on_delete=models.PROTECT, related_name='doc_details')
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    amount = models.DecimalField(
        max_digits=PRODUCT_AMOUNT_MAX_DIGITS,
        decimal_places=PRODUCT_AMOUNT_DECIMAL_PLACES,
    )
