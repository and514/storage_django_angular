from django.contrib.auth.models import User
from rest_framework import fields
from rest_framework import serializers

from core.consts import AMOUNT_MAX_DIGITS, AMOUNT_DECIMAL_PLACES
from shop.models import Measure, Product, Shop, DocType, DocumentDetails, Document
from users.consts import DOCUMENT_NUMBER_MAX_LEN
from users.serializers import UserSerializer


class MeasureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Measure
        fields = ('id', 'name')


class ProductSerializer(serializers.ModelSerializer):
    measure = MeasureSerializer(many=False, read_only=True)
    measure_id = serializers.PrimaryKeyRelatedField(source='measure', queryset=Measure.objects.all())

    class Meta:
        model = Product
        fields = ('id', 'name', 'measure_id', 'measure')


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ('id', 'name')


class DocTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocType
        fields = ('id', 'name')


class DocumentSerializer(serializers.ModelSerializer):
    number = fields.CharField(max_length=DOCUMENT_NUMBER_MAX_LEN)
    date = fields.DateField()

    shop = ShopSerializer(many=False, read_only=True)
    shop_id = serializers.PrimaryKeyRelatedField(source='shop', queryset=Shop.objects.all())
    type = DocTypeSerializer(many=False, read_only=True)
    type_id = serializers.PrimaryKeyRelatedField(source='type', queryset=DocType.objects.all())
    manager = UserSerializer(many=False, read_only=True)
    manager_id = serializers.PrimaryKeyRelatedField(source='manager', queryset=User.objects.all())

    class Meta:
        model = Document
        fields = ('id', 'number', 'date', 'shop_id', 'shop', 'type_id', 'type', 'manager_id', 'manager', 'positions')


class DocumentDetailsSerializer(serializers.ModelSerializer):
    document = DocumentSerializer(many=False, read_only=True)
    document_id = serializers.PrimaryKeyRelatedField(source='document', queryset=Document.objects.all())

    product = ProductSerializer(many=False, read_only=True)
    product_id = serializers.PrimaryKeyRelatedField(source='product', queryset=Product.objects.all())
    amount = fields.DecimalField(max_digits=AMOUNT_MAX_DIGITS, decimal_places=AMOUNT_DECIMAL_PLACES)

    class Meta:
        model = DocumentDetails
        fields = ('id', 'product_id', 'product', 'amount', 'document', 'document_id')
