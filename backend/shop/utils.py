from typing import Tuple

from core.helpers.decorators import postgresql_raw


@postgresql_raw
def get_documents_count(request) -> Tuple[str, dict]:
    from_ = _get_from()
    where_, search = _get_where(request)
    sql_text = """
        SELECT COUNT(*) 
            {0}
            {1}
        """.format(from_, where_)
    params = {
        'search_': search,
    }
    return sql_text, params


@postgresql_raw
def _get_documents(request) -> Tuple[str, dict]:
    size = int(request.GET.get('size', '1'))
    start = (int(request.GET.get('page', '1')) - 1) * size
    from_ = _get_from()
    where_, search = _get_where(request)
    sql_text = """
        SELECT 
            doc.id, doc.number, doc.date, shop.name AS shop_name, doctype.name AS doctype_name, 
            auth_user.username AS manager_name 
            {0}
            {1}
            ORDER BY date DESC, number DESC
            OFFSET %(offset_)s
            LIMIT %(limit_)s
        """.format(from_, where_)
    params = {
        'offset_': start,
        'limit_': size,
        'search_': search,
    }
    return sql_text, params


def _get_from():
    return """            
            FROM shop_document AS doc 
            INNER JOIN shop_shop AS shop ON doc.shop_id = shop.id 
            INNER JOIN shop_doctype AS doctype ON doc.type_id = doctype.id 
            INNER JOIN auth_user ON doc.manager_id = auth_user.id 
    """


def _get_where(request):
    search = request.GET.get('search', "")
    if search:
        fields = ("doc.number", "shop.name", "doctype.name", "auth_user.username")
        where_conditions = " OR ".join([" {0} LIKE %(search_)s ".format(f) for f in fields])
        where = " WHERE {0} ".format(where_conditions)
        search_ = "%{0}%".format(search)
    else:
        where = ""
        search_ = ""
    return where, search_


def get_documents(request):
    results = _get_documents(request)
    fields = ('id', 'number', 'date', 'shop_name', 'doctype_name', 'manager_name')
    return [{f: r[i] for i, f in enumerate(fields)} for r in results]
