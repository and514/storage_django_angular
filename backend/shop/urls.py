from django.urls import include, path, re_path
from rest_framework import routers

from shop import views

router = routers.DefaultRouter()
router.register(r'measure', views.MeasureViewSet)
router.register(r'product', views.ProductViewSet)
router.register(r'shop', views.ShopViewSet)
router.register(r'doc_type', views.DocTypeViewSet)
router.register(r'document', views.DocumentViewSet)
router.register(r'document_details', views.DocumentDetailsViewSet)

urlpatterns = [
    path('', include(router.urls)),
    re_path(r'measure_list', views.MeasureListView.as_view()),
    re_path(r'shop_list', views.ShopListView.as_view()),
    re_path(r'product_list', views.ProductListView.as_view()),
    re_path(r'doc_type_list', views.DocTypeListView.as_view()),
    re_path(r'doc_raw_sql', views.document_raw_view),
]
