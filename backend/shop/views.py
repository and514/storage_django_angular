from django.http import JsonResponse
from rest_framework import generics, status
from rest_framework import viewsets, permissions, filters
from rest_framework.decorators import api_view, permission_classes

from backend.views import WebClientPageNumberPagination
from shop.models import Measure, Product, Shop, DocType, Document, DocumentDetails
from shop.serializers import MeasureSerializer, ProductSerializer, ShopSerializer, DocTypeSerializer, \
    DocumentSerializer, DocumentDetailsSerializer
from shop.utils import get_documents, get_documents_count


class MeasureViewSet(viewsets.ModelViewSet):
    queryset = Measure.objects.all().order_by('name')
    serializer_class = MeasureSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = WebClientPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class MeasureListView(generics.ListAPIView):
    queryset = Measure.objects.all().order_by('name')
    serializer_class = MeasureSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all().select_related('measure').order_by('name')
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = WebClientPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', 'measure__name']


class ProductListView(generics.ListAPIView):
    queryset = Product.objects.all().select_related('measure').order_by('name')
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None


class ShopViewSet(viewsets.ModelViewSet):
    queryset = Shop.objects.all().order_by('name')
    serializer_class = ShopSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = WebClientPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class ShopListView(generics.ListAPIView):
    queryset = Shop.objects.all().order_by('name')
    serializer_class = ShopSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None


class DocTypeViewSet(viewsets.ModelViewSet):
    queryset = DocType.objects.all().order_by('name')
    serializer_class = DocTypeSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = WebClientPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']


class DocTypeListView(generics.ListAPIView):
    queryset = DocType.objects.all().order_by('name')
    serializer_class = DocTypeSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None


class DocumentViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all().prefetch_related('shop', 'type', 'manager').order_by('-date', '-number')
    serializer_class = DocumentSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = WebClientPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['number', 'shop__name', 'type__name', 'manager__username']


class DocumentDetailsViewSet(viewsets.ModelViewSet):
    queryset = DocumentDetails.objects.all().prefetch_related('product', 'product__measure').order_by('product__name')
    serializer_class = DocumentDetailsSerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = WebClientPageNumberPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['product__name', 'measure__name']

    def get_queryset(self):
        q = super(DocumentDetailsViewSet, self).get_queryset()
        document_id = self.request.query_params.get('document_id')
        if document_id:
            q = q.filter(document_id=document_id)
        return q


@api_view(['GET'])
@permission_classes([permissions.IsAuthenticated])
def document_raw_view(request):
    results = get_documents(request)
    count = get_documents_count(request)

    return JsonResponse(
        {'results': results, 'count': count}, status=status.HTTP_200_OK
    )
