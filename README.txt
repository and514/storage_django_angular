# **********************************************************************************************************************
# Запуск приложения
# **********************************************************************************************************************
# запустить Docker (!!! должен быть установлен docker и docker-compose)
sudo docker-compose up --build
# открыть в браузере
http://localhost:8007

#  остановить контейнеры
sudo docker-compose stop && docker-compose rm -f
#  удалить контейнеры
sudo docker ps -a | grep "storage_django_angular" | awk '{print $1}' | xargs sudo docker rm
# **********************************************************************************************************************

# Дамп БД с небольшим набором данных можно взять из ./dumps
# !!! Должен быть установлен Postgresql
# Восстановить можно так:
psql --host "localhost" --port "5433" --username "new_project_user" --dbname "new_project_db" -c "DROP SCHEMA public CASCADE; CREATE SCHEMA public; GRANT ALL ON SCHEMA public TO public; GRANT ALL ON SCHEMA public TO new_project_user;"
# пароль: new_project_pass
/usr/bin/pg_restore --host "localhost" --port "5433" --username "new_project_user" --role "new_project_user" --dbname "new_project_db" --no-owner --verbose ./dumps/storage_django_angular_dump.backup

# снаружи докера доступен порт PostgresQL - 5433

# Пользователи (из дампа)
admin:admin
user:GBp9ADiSXjR


# для отладки


# 1. Docker
 sudo docker-compose up

# 2. скрипт - Python
 Script path - /полный путь/storage_django_angular/backend/manage.py
 Parameters - runserver 0.0.0.0:8015
 Environment:
 POSTGRES_HOST=localhost
 POSTGRES_PORT='5433'

# 3. Angular
 npm
 package.json -/полный путь/storage_django_angular/frontend/package.json
 Command - run
 Scripts - start



