import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {InitService} from './shared/services/init.service';
import {SharedModule} from './shared/shared.module';
import {HttpXsrfInterceptor} from './shared/services/csrf.service';
import {AppRoutingModule} from './app-routing.module';
import {AuthModule} from './components/auth';
import {ClarityModule} from '@clr/angular';
import {MenuComponent} from './shared/components/menu';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DatePipe} from '@angular/common';
import {ComponentsModule} from './components/components.module';

registerLocaleData(localeFr);

export function initFactory(service: InitService): () => Promise<any> {
  return () => service.init();
}

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    // CSRF 1 step
    HttpClientXsrfModule.withOptions({cookieName: 'csrftoken', headerName: 'x-csrftoken'}),
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ClarityModule,
    // App modules
    AuthModule,
    SharedModule.forRoot(),
    ComponentsModule,
  ],
  providers: [
    InitService,
    { provide: APP_INITIALIZER, useFactory: initFactory, deps: [InitService], multi: true },
    // CSRF 2 step
    { provide: HTTP_INTERCEPTORS, useClass: HttpXsrfInterceptor, multi: true },
    DatePipe,
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
