import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {
  apiUrl = environment.host + '/api/shop/product/';
  fields = [
    {name: 'id', title: 'Id', readOnly: true},
    {name: 'name', title: 'Наименование'},
    {name : 'measure_id', title: 'Единица измерения'},
  ];
  editFormSelects = {
    measure_id: {source: 'measure', key: 'id', value: 'name', apiUrl: environment.host + '/api/shop/measure_list/'}
  }
}
