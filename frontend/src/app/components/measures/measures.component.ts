import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-measures',
  templateUrl: './measures.component.html',
  styleUrls: ['./measures.component.scss']
})
export class MeasuresComponent {
  apiUrl = environment.host + '/api/shop/measure/';
  fields = [
    {name: 'id', title: 'Id', var: 'id', readOnly: true},
    {name: 'name', title: 'Наименование', var: 'name'},
  ];
}
