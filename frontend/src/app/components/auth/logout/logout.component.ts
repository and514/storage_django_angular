import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {ApiService} from '../../../shared/services/api.service';
import {AuthService} from '../../../shared/services/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit, OnDestroy {
  public title = 'Fraud Detect Platform';
  private apiLogoutSubs: Subscription = null;

  constructor(
    private api: ApiService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.logout();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
  }

  unsubscribe(): void {
    if (this.apiLogoutSubs !== null) {
      this.apiLogoutSubs.unsubscribe();
    }
  }

  // выход
  logout(): void {
    this.unsubscribe();
    this.apiLogoutSubs = this.api.apiLogout().subscribe(
      res => {
        if (res) {
          this.authService.clear();
          // noinspection JSIgnoredPromiseFromCall
          this.router.navigateByUrl('/');
        }
      },
    );
  }
}
