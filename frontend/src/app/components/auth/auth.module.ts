import {NgModule} from '@angular/core';

import {LoginComponent} from './login/login.component';
import {ClrCommonFormsModule, ClrInputModule, ClrPasswordModule, ClrSelectModule} from '@clr/angular';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import { LogoutComponent } from './logout/logout.component';


@NgModule({
    imports: [
        ClrCommonFormsModule,
        ClrInputModule,
        ReactiveFormsModule,
        ClrPasswordModule,
        ClrSelectModule,
        CommonModule
    ],
  declarations: [
    LoginComponent,
    LogoutComponent
  ],
  providers: [
  ],
})
export class AuthModule {
}
