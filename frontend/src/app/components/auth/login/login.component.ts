import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {AuthService} from '../../../shared/services/auth.service';
import {ApiService} from '../../../shared/services/api.service';
import {setUserInfo} from '../../../shared/functions/localstorage';

export interface User {
  username: string;
  password: string;
}


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  private loginSubs: Subscription = null;
  private isError = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private api: ApiService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnDestroy(): void {
    this._loginUnsubscribe();
  }

  // признак корректности данных
  get isWrongData(): boolean {
    return this.isError;
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this._clear();
      this.loginSubscribe(this.form.value);
    }
  }

  // подписка на логин
  private loginSubscribe(user: User): void {
    this._loginUnsubscribe();
    const params = {
      username: user.username,
      password: user.password,
    };
    this.loginSubs = this.api.apiLogin(params).subscribe(
      res => {
        if (res.result && res.result.auth === true) {
          setUserInfo(res.result);
          this.loginOk();
        }
      },
      () => {
        this.isError = true;
      }
    );
  }

  private loginOk(): void {
    this.authService.setLoggedIn(true);
    // noinspection JSIgnoredPromiseFromCall
    this.router.navigateByUrl('/');
  }

  private _loginUnsubscribe(): void {
    if (this.loginSubs !== null) {
      // отписка логин
      this.loginSubs.unsubscribe();
    }
  }

  // очистка
  private _clear(): void {
    // очистка аутентификации
    this.authService.clear();
  }

}
