import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsRawSqlComponent } from './docs-raw-sql.component';

describe('DocsRawSqlComponent', () => {
  let component: DocsRawSqlComponent;
  let fixture: ComponentFixture<DocsRawSqlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocsRawSqlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsRawSqlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
