import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-docs-raw-sql',
  templateUrl: './docs-raw-sql.component.html',
  styleUrls: ['./docs-raw-sql.component.scss']
})
export class DocsRawSqlComponent {
  apiUrl = environment.host + '/api/shop/doc_raw_sql/';

  isAddRowEnabled = false
  isEditRowEnabled = false
  isDeleteEnabled = false

   fields = [
    {name : 'id', title: 'Id', readOnly: true},
    {name : 'number', title: 'Номер'},
    {name : 'date', title: 'Дата', type: 'DATE'},
    {name : 'shop_name', title: 'Магазин'},
    {name : 'doctype_name', title: 'Тип документа'},
    {name : 'manager_name', title: 'Менеджер'},
  ];
}
