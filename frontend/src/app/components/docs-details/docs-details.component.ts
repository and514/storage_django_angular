import {Component, OnDestroy, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {unSubscribe} from '../../shared/functions/unsubscribe';


@Component({
  selector: 'app-docs-details',
  templateUrl: './docs-details.component.html',
  styleUrls: ['./docs-details.component.scss']
})
export class DocsDetailsComponent implements OnInit, OnDestroy {
  private _subscriptionRoute: Subscription = null;
  private docID: number;

  apiUrl = environment.host + '/api/shop/document_details/';

  fields = [
    // {name: 'id', title: 'Id', readOnly: true},
    {name: 'product_id', title: 'Наименование товара'},
    {name: 'amount', title: 'Количество'},
  ];
  editFormSelects = {
    product_id: {source: 'product', key: 'id', value: 'name', apiUrl: environment.host + '/api/shop/product_list/'},
  }
  eventCallBacks = {
    beforeCreateRequest: this.addDocId,
    beforeUpdateRequest: this.addDocId,
    beforeListRequest: this.addDocId,
  }

  constructor(
    private _activateRoute: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    localStorage.removeItem('DOC_ID');
    this._routeParamsSubscribe();
  }

  ngOnDestroy(): void {
    [this._subscriptionRoute].map(subs => {
      unSubscribe(subs);
    })
  }

  private _routeParamsSubscribe(): void {
    // параметры из роута
    this._subscriptionRoute = this._activateRoute.params.subscribe(res => {
      this.docID = res.doc_id;
      localStorage.setItem('DOC_ID', String(res.doc_id));
    });
  }

  public returnToDocList(): void {
    this.router.navigate([environment.host + '/api/shop/document/']);
  }

  protected addDocId(data, scope): object {
    data.document_id = localStorage.getItem('DOC_ID');
    return data;
  }
}
