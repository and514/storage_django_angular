import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-doc-types',
  templateUrl: './doc-types.component.html',
  styleUrls: ['./doc-types.component.scss']
})
export class DocTypesComponent {
  apiUrl = environment.host + '/api/shop/doc_type/';
  fields = [
    {name: 'id', title: 'Id', var: 'id', readOnly: true},
    {name: 'name', title: 'Наименование', var: 'name'},
  ];
}
