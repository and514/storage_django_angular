import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-shops',
  templateUrl: './shops.component.html',
  styleUrls: ['./shops.component.scss']
})
export class ShopsComponent {
  apiUrl = environment.host + '/api/shop/shop/';
  fields = [
    {name: 'id', title: 'Id', var: 'id', readOnly: true},
    {name: 'name', title: 'Наименование', var: 'name'},
  ];
}
