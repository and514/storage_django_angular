import {ModuleWithProviders, NgModule} from '@angular/core';
import {MeasuresComponent} from './measures/measures.component';
import {ProductsComponent} from './products/products.component';
import {ShopsComponent} from './shops/shops.component';
import {DocTypesComponent} from './doc-types/doc-types.component';
import {DocsOrmComponent} from './docs-orm/docs-orm.component';
import {ClrCommonFormsModule, ClrDatagridModule, ClrIconModule, ClrInputModule} from '@clr/angular';
import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {DocsRawSqlComponent} from './docs-raw-sql/docs-raw-sql.component';
import {DocsDetailsComponent} from './docs-details/docs-details.component';

@NgModule({
    imports: [
        ClrDatagridModule,
        SharedModule,
        ClrCommonFormsModule,
        ReactiveFormsModule,
        ClrInputModule,
        CommonModule,
        ClrIconModule
    ],
  exports: [
  ],
  declarations: [
    MeasuresComponent,
    ProductsComponent,
    ShopsComponent,
    DocTypesComponent,
    DocsOrmComponent,
    DocsRawSqlComponent,
    DocsDetailsComponent,
  ],
  providers: [
  ],
})
export class ComponentsModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: ComponentsModule,
      providers:  [
      ]
    };
  }
}
