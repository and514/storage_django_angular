import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsOrmComponent } from './docs-orm.component';

describe('DocsOrmComponent', () => {
  let component: DocsOrmComponent;
  let fixture: ComponentFixture<DocsOrmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocsOrmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsOrmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
