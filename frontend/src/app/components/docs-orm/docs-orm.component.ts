import {Component} from '@angular/core';
import {environment} from '../../../environments/environment';
import {getUserId, getUserName} from '../../shared/functions/localstorage';

@Component({
  selector: 'app-docs-orm',
  templateUrl: './docs-orm.component.html',
  styleUrls: ['./docs-orm.component.scss']
})
export class DocsOrmComponent {
  apiUrl = environment.host + '/api/shop/document/';
  isShowDetailsEnabled = true;
  detailsComponentParams = {
    url: '/docs/orm/details',
    key: 'id',
  };

  fields = [
    {name : 'id', title: 'Id', readOnly: true},
    {name : 'number', title: 'Номер'},
    {name : 'date', title: 'Дата', type: 'DATE'},
    {name : 'shop_id', title: 'Магазин'},
    {name : 'type_id', title: 'Тип документа'},
    {name : 'positions', title: 'Кол-во позиций', readOnly: true},
    {name : 'manager_id', title: 'Менеджер', display: this.getManagerName},
  ];
  editFormSelects = {
    shop_id: {source: 'shop', key: 'id', value: 'name', apiUrl: environment.host + '/api/shop/shop_list/'},
    type_id: {source: 'type', key: 'id', value: 'name', apiUrl: environment.host + '/api/shop/doc_type_list/'},
  }
  eventCallBacks = {
    beforeCreateRequest: this.addManagerId,
    beforeUpdateRequest: this.addManagerId,
  }

  protected getManagerName(field, item, scope): string {
    if (scope.mode === scope.modes.AddRow) {
      return getUserName();
    }
    if (scope.mode === scope.modes.EditRow) {
      return scope.updateFields.manager.username;
    }
    return item.manager.username;
  }

  protected addManagerId(data): object {
    data.manager_id = getUserId();
    return data;
  }
}
