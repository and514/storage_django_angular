export function reverseDAteFormat(date: string, sep: string = '/'): string {
    return date.replace('-', sep).split(sep).reverse().join('-');
  }
