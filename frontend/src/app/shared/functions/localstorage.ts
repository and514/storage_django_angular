const CURRENT_USER_ID = 'currentUserId';
const CURRENT_USER_NAME = 'currentUserName';
const GRID_PAGE_SIZE = 'gridPageSize';

export function clearLocalStorage(): void {
  localStorage.removeItem(CURRENT_USER_ID);
  localStorage.removeItem(CURRENT_USER_NAME);
}

export interface UserInfo {
  user_id: number;
  user_name: string;
}

// UserInfo
export function setUserInfo(obj: UserInfo): void {
  localStorage.setItem(CURRENT_USER_ID, String(obj.user_id));
  localStorage.setItem(CURRENT_USER_NAME, String(obj.user_name));
}
export function getUserId(): string {
  return localStorage.getItem(CURRENT_USER_ID);
}
export function getUserName(): string {
  return localStorage.getItem(CURRENT_USER_NAME);
}

// Grid
export function setGridPageSize(val): void {
  localStorage.setItem(GRID_PAGE_SIZE, String(val));
}
export function getGridPageSize(): string {
  return localStorage.getItem(GRID_PAGE_SIZE)
}

