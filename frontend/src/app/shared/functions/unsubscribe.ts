export function unSubscribe (subs): void {
    if (subs !== null && subs !== undefined) {
      subs.unsubscribe();
    }
}
