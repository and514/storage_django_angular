import {Injectable} from '@angular/core';

import {HttpApiService} from './http-api.service';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class ApiService {
  constructor(
    private httpApi: HttpApiService
  ) { }

  // api - инициализация приложения
  public getCSRFToken(): Observable<any> {
    return this.httpApi.get(environment.initUrl);
  }

  // api - вход
  public apiLogin(params): Observable<any> {
    return this.httpApi.post(environment.loginUrl, params);
  }

  // api - выход
  public apiLogout(): Observable<any> {
    return this.httpApi.post(environment.logoutUrl);
  }

  public List(url:string, params: object): Observable<any> {
    return this.httpApi.get(url, params);
  }

  public Create(url:string, params: object): Observable<any> {
    return this.httpApi.post(url, params);
  }

  public Update(url:string, params: object, id: number): Observable<any> {
    const fullUrl = url + id + '/';
    return this.httpApi.put(fullUrl, params);
  }

  public Delete(url:string, id: number): Observable<any> {
    const fullUrl = url + id + '/';
    return this.httpApi.delete(fullUrl);
  }
}
