import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {AuthService} from './auth.service';
import {clearLocalStorage, setUserInfo} from '../functions/localstorage';

@Injectable()
export class InitService {

  constructor(
    private api: ApiService,
    private authService: AuthService,
  ) { }

  public init(): Promise<any> {
    // обнулить сторадж
    clearLocalStorage();
    // получение куки с токеном
    const promise = this.api.getCSRFToken().toPromise();
    promise.then((resp) => {
      setUserInfo(resp.result);
      this.authService.setLoggedIn(resp.result.auth);
    });
    return promise;
  }
}
