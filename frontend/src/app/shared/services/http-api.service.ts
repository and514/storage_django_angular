import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Params } from '@angular/router';

@Injectable()
export class HttpApiService {
  constructor(
    private http: HttpClient,
  ) { }

  // Сборка параметров для запроса
  static buildHttpParams(params: Params): HttpParams {
    let httpParams: HttpParams = new HttpParams();
    for (const key in params) {
      if (params[key] || params[key] === '') {
        httpParams = httpParams.set(key, params[key]);
      }
    }
    return httpParams;
  }

  public get(url: string, params: Params = null): Observable<any> {
    const httpParams: HttpParams = HttpApiService.buildHttpParams(params);
    return this.http.get(url, {
      params: httpParams,
      withCredentials: true
    });
  }

  public post(url: string, params: Params = null, headers: HttpHeaders = null): Observable<any> {
    return this.http.post(url, params, {headers, withCredentials: true});
  }

  public put(url: string, params: Params = null, headers: HttpHeaders = null): Observable<any> {
    return this.http.put(url, params, {headers, withCredentials: true});
  }

  public delete(url: string, headers: HttpHeaders = null): Observable<any> {
    return this.http.delete(url, {headers, withCredentials: true});
  }
}
