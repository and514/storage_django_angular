import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class AuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() {}

  public get isLoggedIn(): boolean {
    return this.loggedIn.getValue();
  }

  public setLoggedIn(val: boolean): void {
    this.loggedIn.next(val);
  }

  public clear(): void {
    this.loggedIn.next(false);
  }

}
