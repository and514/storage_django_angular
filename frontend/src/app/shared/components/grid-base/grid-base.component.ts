import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {getDefaultPageSize} from '../grid-paging/grid-paging.component';
import {ApiService} from '../../services/api.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {unSubscribe} from '../../functions/unsubscribe';
import {reverseDAteFormat} from '../../functions/dates';
import {Router} from '@angular/router';


export function getDefaultGridPageParams(): object {
  return {page: 1, size: getDefaultPageSize(), firstRow: 1};
}

@Component({
  selector: 'app-grid-base',
  templateUrl: './grid-base.component.html',
  styleUrls: ['./grid-base.component.css']
})
export class GridBaseComponent implements OnDestroy, OnInit {

  constructor(
    protected apiService: ApiService,
    private fbEditForm: FormBuilder,
    private router: Router,
  ) { }

  protected subsList: Subscription = null;
  protected subsCreate: Subscription = null;
  protected subsUpdate: Subscription = null;
  protected subsDelete: Subscription = null;
  public modes = {
    List: 1,
    AddRow: 2,
    EditRow: 3,
  };
  public mode = this.modes.List;

  // List
  public loadingList = true;
  public dataList = [];
  public totalItems = 0;
  public pagingParams: { };  // используется в дочерних классах

  // EditForm
  public editForm: FormGroup;
  public isErrorCreate = false;
  protected updateFields = {};

  // Search
  public searchForm: FormGroup;

  // параметры класса
  @Input() apiUrl: string;
  @Input() fields: Array<object>;
  @Input() eventCallBacks: object;
  @Input() editFormSelects: object;

  @Input() isSearchEnabled = true;
  @Input() isAddRowEnabled = true;
  @Input() isEditRowEnabled = true;
  @Input() isDeleteEnabled = true;

  @Input() isShowDetailsEnabled = false;
  @Input() detailsComponentParams: object;

  ngOnInit() {
    this.initEditForm();
    this.initSearchForm();
  }

  ngOnDestroy(): void {
    [this.subsList, this.subsCreate, this.subsUpdate, this.subsDelete].map(subs => {
      unSubscribe(subs);
    })
  }

  private initEditForm() {
    const groupControls = {};
    for (const field of this.fields) {
      if (field.hasOwnProperty('readOnly') && field['readOnly'] === true || field.hasOwnProperty('display')) {
        groupControls[field['name']] = new FormControl({value: null, disabled: true}, Validators.required);
      } else {
        groupControls[field['name']] = ['', {validators: Validators.required}];
      }
    }
    this.editForm = this.fbEditForm.group({
      ...groupControls,
    });
  }

  // List
  public pagingFormChanges(pageParams): void {
    this.pagingParams = pageParams;
    this.loadDataList();
  }
  public loadDataList(): void {
    unSubscribe(this.subsList);
    this.loadingList = true;
    let params = Object.assign({}, this.pagingParams, this.searchForm.value);
    params = this.callBackRun('beforeListRequest', params);
    this.subsList = this.apiService.List(this.apiUrl, params).subscribe(res => {
      if (res) {
        this.dataList = this.callBackRun('afterLoadDataList', res.results);
        this.totalItems = res.count;
        this.loadingList = false;
      }
    });
  }

  // EditForm
  public submitEditForm() {
    if (!this.editForm.valid) {
      return;
    }
    if (this.mode === this.modes.AddRow) {
      this.create(this.editForm.value);
    }
    if (this.mode === this.modes.EditRow) {
      this.update(this.editForm.value);
    }
  }

  // Search
  initSearchForm(): void {
    this.searchForm = new FormGroup({
      search: new FormControl(''),
    });
    this.onSearchFormChanges();
  }
  onSearchFormChanges(): void {
    this.searchForm.valueChanges.subscribe(() => {
      if (!this.searchForm.value.search) {
        this.pagingParams = getDefaultGridPageParams();
      }
      this.loadDataList();
    });
  }

  // Create
  public addRow(): void {
    this.editForm.reset();
    this.setMode(this.modes.AddRow);
  }
  // подписка на создание записи
  private create(params): void {
    unSubscribe(this.subsCreate);
    params = this.formatParamsBeforeRequest(params);
    params = this.callBackRun('beforeCreateRequest', params);
    this.subsCreate = this.apiService.Create(this.apiUrl, params).subscribe(res => {
        if (res) {
          this.setMode(this.modes.List);
          this.isErrorCreate = false;
          this.loadDataList();
        }
      },
      () => {
        this.isErrorCreate = true;
      }
    );
  }

  // UpdateForm
  public editRow(item): void {
    this.updateFields = {...item};
    const updateFields = {};
    const controls = this.editForm.controls;
    Object.keys(controls).map((objectKey) => {
      // Date
      if (objectKey.includes('date')) {
        updateFields[objectKey] = reverseDAteFormat(item[objectKey]);
      } else {
        updateFields[objectKey] = item[objectKey];
      }
    });
    this.editForm.setValue(updateFields);
    this.setMode(this.modes.EditRow);
  }
  // подписка на обновление записи
  private update(params): void {
    unSubscribe(this.subsUpdate);
    let updateFields = Object.assign(
      this.updateFields,
      this.formatParamsBeforeRequest(params));
    updateFields = this.callBackRun('beforeUpdateRequest', updateFields);
    // @ts-ignore
    this.subsUpdate = this.apiService.Update(this.apiUrl, updateFields, updateFields.id).subscribe(res => {
        if (res) {
          this.setMode(this.modes.List);
          this.isErrorCreate = false;
          this.loadDataList();
        }
      },
      () => {
        this.isErrorCreate = true;
      }
    );
  }

  private formatParamsBeforeRequest(params: object): object {
    Object.keys(params).map((fieldName) => {
      // Date
      if (fieldName.includes('date')) {
        params[fieldName] = reverseDAteFormat(params[fieldName]);
      }
    });
    return params;
  }

  // Delete
  public deleteRow(item): void {
    this.delete(item);
  }
  // подписка на удаление записи
  private delete(params): void {
    unSubscribe(this.subsDelete);
    this.subsDelete = this.apiService.Delete(this.apiUrl, params.id).subscribe(() => {
        this.setMode(this.modes.List);
        this.isErrorCreate = false;
        this.loadDataList();
      },
      () => {
        this.isErrorCreate = true;
      }
    );
  }

  public setMode(mode) {
    const oldMode = this.mode;
    this.mode = mode;
    // если включается режим редактирвания, нужно проверить импорт справочников
    if ([this.modes.AddRow, this.modes.EditRow].includes(mode)) {
      this.loadSelects();
    }
    console.log('Change mode from ' + oldMode + ' to ' + mode);
    this.callBackRun('afterModeChanged', {oldMode, mode, scope: this});
  }

  // Selects
  private loadSelects(): void {
    if (!this.editFormSelects) {
      return;
    }
    for (const selectKey of Object.keys(this.editFormSelects)) {
      this.loadSelectOptions(selectKey);
    }
  }
  private loadSelectOptions(selectKey: string): void {
    const select = this.editFormSelects[selectKey];
    if(select.dataList) {
      return;
    }
    unSubscribe(select.subs);
    select.subs = this.apiService.List(select.apiUrl, null).subscribe(res => {
      if (res) {
        if (res.results) {
          select.dataList = res.results;
        } else {
          select.dataList = res;
        }
      }
    });
  }
  public getSelectOptions(nameField): any {
    const select = this.editFormSelects[nameField];
    return select.dataList || [];
  }

  private callBackRun(callBackName, data) {
    if (this.eventCallBacks && this.eventCallBacks.hasOwnProperty(callBackName)) {
      return this.eventCallBacks[callBackName](data, this);
    }
    return data;
  }

  public getTableCellValue(item, field): string {
    // Select
    if (this.editFormSelects && this.editFormSelects.hasOwnProperty(field.name)) {
      const select: object = this.editFormSelects[field.name];
      return item[select['source']][select['value']];
    }
    // View only property
    if (field.hasOwnProperty('display')) {
      return field['display'](field, item, this);
    }
    return item[field.name];
  }


  // Details
  public showDetailsRow(item): void {
    this.router.navigate([this.detailsComponentParams['url'], item[this.detailsComponentParams['key']]]);
  }
}
