import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {getGridPageSize, setGridPageSize} from '../../functions/localstorage';

export const Sizes = [10, 20, 50, 100];

export function getDefaultPageSize(): number {
  return Number(getGridPageSize() || Sizes[2]);
}

@Component({
  selector: 'app-grid-paging',
  templateUrl: './grid-paging.component.html',
  styleUrls: ['./grid-paging.component.css']
})
export class GridPagingComponent implements OnInit {
  @Input() totalItems: number;
  @Output() public pagingFormChanges: EventEmitter<any> = new EventEmitter();
  sizes = Sizes;
  pagingForm: FormGroup;

  ngOnInit(): void {
    this.pagingForm = new FormGroup({
      page: new FormControl(1),
      size: new FormControl(getDefaultPageSize()),
    });
    this.onPagingFormChanges();
  }

  public get firstItem(): number{
    return (this.pagingForm.value.page - 1) * this.pagingForm.value.size + 1;
  }

  public get lastItem(): number{
    return Math.min(this.firstItem + this.pagingForm.value.size - 1, this.totalItems);
  }

  public get sizeCurrentPageField(): number{
    return String(this.totalItems).length;
  }

  public setFirstPage(): void{
    return this.pagingForm.patchValue({page: 1});
  }

  public get totalPages(): number{
    return Math.ceil(this.totalItems / this.pagingForm.value.size);
  }

  public setLastPage(): void{
    return this.pagingForm.patchValue({page: this.totalPages});
  }

  public setPrevPage(): void{
    return this.pagingForm.patchValue({page: parseInt(this.pagingForm.value.page, 10) - 1});
  }

  public setNextPage(): void{
    return this.pagingForm.patchValue({page: parseInt(this.pagingForm.value.page, 10) + 1});
  }

  private callPagingFormChanges(): void {
      const params = Object.assign({firstRow: this.firstItem}, this.pagingForm.value);
      this.pagingFormChanges.emit(params);
  }

  onPagingFormChanges(): void {
    this.callPagingFormChanges();
    this.pagingForm.valueChanges.subscribe(() => {

      if (!Number(this.pagingForm.value.page) || this.pagingForm.value.page < 1) {
        this.setFirstPage();
        return;
      }
      if (this.pagingForm.value.page > this.totalPages) {
        this.setLastPage();
        return;
      }

      setGridPageSize(this.pagingForm.value.size);
      this.callPagingFormChanges();
    });
  }
}
