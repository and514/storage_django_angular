import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {getUserName} from '../../functions/localstorage';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent {
  public getUserName = getUserName;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  public isLoggedIn(): boolean {
    return this.authService.isLoggedIn;
  }

  public isParentUrl(url: string): boolean {
    return this.router.routerState.snapshot.url.startsWith(url);
  }
}
