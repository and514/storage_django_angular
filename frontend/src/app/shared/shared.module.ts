import {ModuleWithProviders, NgModule} from '@angular/core';
import {GridPagingComponent} from './components/grid-paging/grid-paging.component';
import {
  ClrCommonFormsModule,
  ClrDatagridModule,
  ClrDatepickerModule,
  ClrIconModule,
  ClrInputModule,
  ClrSelectModule
} from '@clr/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {GridBaseComponent} from './components/grid-base/grid-base.component';
import {AuthService} from './services/auth.service';
import {AuthGuard} from './services/auth.guard';
import {ApiService} from './services/api.service';
import {HttpApiService} from './services/http-api.service';
import {SanitizeHtmlPipe} from './pipes/sanitize-html';

@NgModule({
  imports: [
    ClrIconModule,
    ReactiveFormsModule,
    CommonModule,
    ClrDatagridModule,
    ClrCommonFormsModule,
    ClrInputModule,
    FormsModule,
    ClrSelectModule,
    ClrDatepickerModule,
  ],
  exports: [
    GridPagingComponent,
    GridBaseComponent,
  ],
    declarations: [
        GridPagingComponent,
        GridBaseComponent,
        SanitizeHtmlPipe,
    ],
  providers: [
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: SharedModule,
      providers:  [
        AuthService,
        AuthGuard,
        ApiService,
        HttpApiService,
      ]
    };
  }
}
