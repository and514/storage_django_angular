import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/auth/login/login.component';
import {LogoutComponent} from './components/auth/logout/logout.component';
import {MeasuresComponent} from './components/measures/measures.component';
import {AuthGuard} from './shared/services/auth.guard';
import {ProductsComponent} from './components/products/products.component';
import {ShopsComponent} from './components/shops/shops.component';
import {DocTypesComponent} from './components/doc-types/doc-types.component';
import {DocsOrmComponent} from './components/docs-orm/docs-orm.component';
import {DocsRawSqlComponent} from './components/docs-raw-sql/docs-raw-sql.component';
import {DocsDetailsComponent} from './components/docs-details/docs-details.component';


const DEFAULT_REDIRECT = 'docs/orm';

const routes: Routes = [
  { path: '', redirectTo: DEFAULT_REDIRECT, pathMatch: 'full', canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'dirs', redirectTo: 'dirs/measures', canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'dirs/measures', component: MeasuresComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'dirs/products', component: ProductsComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'dirs/shops', component: ShopsComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'dirs/doc_types', component: DocTypesComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },

  { path: 'docs', redirectTo: 'docs/input', canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'docs/orm', component: DocsOrmComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'docs/orm/details/:doc_id', component: DocsDetailsComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: 'docs/raw', component: DocsRawSqlComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },

  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
  { path: '**', redirectTo: DEFAULT_REDIRECT, canActivate: [AuthGuard], canActivateChild: [AuthGuard] },
];



@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
