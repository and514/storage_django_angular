#!/bin/sh
#set -e

#ETCD_CONFIG=${ETCD_CONFIG:-./services/config/config.docker.yaml}
#cp $ETCD_CONFIG ./services/config/config.yaml
#
#dockerize -wait tcp://clickhouse:8123 -wait tcp://rabbitmq:5672 -wait tcp://db:5432 -wait tcp://redis:6379 -timeout 60s echo "Ready to go..."

#python manage.py collectstatic --noinput
#python manage.py migrate
#cp ./locale/zh_CN/* ./locale/zh_Hant/ -r
#python manage.py compilemessages
#
#python ./scripts/load_fixture.py -f
#python manage.py update_token_pool
#python manage.py metrics_init_schema
#python manage.py get_fiat_exchange_rate
#
#supervisord -n -c /etc/supervisor/supervisor.conf
