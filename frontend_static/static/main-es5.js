(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "./$$_lazy_route_resource lazy recursive":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function $$_lazy_route_resourceLazyRecursive(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/auth/login/login.component.html":
    /*!**************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/auth/login/login.component.html ***!
      \**************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsAuthLoginLoginComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n              <div class=\"login-wrapper\">\n                  <form class=\"login\" [formGroup]=\"form\" autocomplete=\"off\">\n                      <section class=\"title\">\n                          <h3 class=\"welcome\">Welcome to</h3>\n                          Shop\n                      </section>\n                      <div class=\"login-group\">\n                          <clr-input-container>\n                              <label class=\"clr-sr-only\">Username</label>\n                              <input type=\"text\" name=\"username\" clrInput placeholder=\"Username\" formControlName=\"username\"/>\n                          </clr-input-container>\n                          <clr-password-container>\n                              <label class=\"clr-sr-only\">Password</label>\n                              <input type=\"password\" name=\"password\" clrPassword placeholder=\"Password\" formControlName=\"password\"/>\n                          </clr-password-container>\n                          <div class=\"error active\" *ngIf=\"isWrongData\">\n                              Invalid user name or password\n                          </div>\n                          <button type=\"submit\" class=\"btn btn-primary\" (click)=\"onSubmit()\">NEXT</button>\n                      </div>\n                  </form>\n              </div>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/auth/logout/logout.component.html":
    /*!****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/auth/logout/logout.component.html ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsAuthLogoutLogoutComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<p>logout</p>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/doc-types/doc-types.component.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/doc-types/doc-types.component.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsDocTypesDocTypesComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-grid-base [apiUrl]=\"apiUrl\" [fields]=\"fields\"></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-details/docs-details.component.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-details/docs-details.component.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsDocsDetailsDocsDetailsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n<div>\n  <button class=\"btn\" type=\"button\" (click)=\"returnToDocList()\" style=\"margin-top: 20px;\">Вернуться к списку документов</button>\n</div>\n\n  <app-grid-base\n  [apiUrl]=\"apiUrl\"\n  [fields]=\"fields\"\n  [editFormSelects]=\"editFormSelects\"\n  [eventCallBacks]=\"eventCallBacks\"\n></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-orm/docs-orm.component.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-orm/docs-orm.component.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsDocsOrmDocsOrmComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-grid-base\n  [apiUrl]=\"apiUrl\"\n  [fields]=\"fields\"\n  [editFormSelects]=\"editFormSelects\"\n  [eventCallBacks]=\"eventCallBacks\"\n  [isShowDetailsEnabled]=\"isShowDetailsEnabled\"\n  [detailsComponentParams]=\"detailsComponentParams\"\n></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-raw-sql/docs-raw-sql.component.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-raw-sql/docs-raw-sql.component.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsDocsRawSqlDocsRawSqlComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-grid-base\n  [apiUrl]=\"apiUrl\"\n  [fields]=\"fields\"\n  [isAddRowEnabled]=\"isAddRowEnabled\"\n  [isEditRowEnabled]=\"isEditRowEnabled\"\n  [isDeleteEnabled]=\"isDeleteEnabled\"\n></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/measures/measures.component.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/measures/measures.component.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsMeasuresMeasuresComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-grid-base [apiUrl]=\"apiUrl\" [fields]=\"fields\"></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/products/products.component.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/products/products.component.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsProductsProductsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-grid-base [apiUrl]=\"apiUrl\" [fields]=\"fields\" [editFormSelects]=\"editFormSelects\"></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shops/shops.component.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/shops/shops.component.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppComponentsShopsShopsComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-grid-base [apiUrl]=\"apiUrl\" [fields]=\"fields\"></app-grid-base>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/grid-base/grid-base.component.html":
    /*!************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/grid-base/grid-base.component.html ***!
      \************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsGridBaseGridBaseComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"card\" *ngIf=\"[modes.AddRow, modes.EditRow].includes(mode)\">\n\n    <form clrForm [formGroup]=\"editForm\">\n          <div *ngFor=\"let field of fields\">\n              <div *ngIf=\"editFormSelects && editFormSelects.hasOwnProperty(field['name']); else otherField\">\n\n                <clr-select-container>\n                  <label>{{field['title']}}</label>\n                  <select clrSelect name=\"options\" [formControlName]=\"field['name']\">\n                    <option *ngFor=\"let selectOption of getSelectOptions(field['name'])\" [ngValue]=\"selectOption[editFormSelects[field['name']]['key']]\">\n                      {{ selectOption[editFormSelects[field['name']]['value']] }}\n                    </option>\n                  </select>\n                </clr-select-container>\n              </div>\n\n              <ng-template #otherField>\n                <div *ngIf=\"field.hasOwnProperty('display'); else otherField_2\" >\n                  <clr-control-container>\n                    <label>{{field['title']}}</label>\n                    <span class=\"clr-input\">{{field['display'](field, editForm.value, this)}}</span>\n                  </clr-control-container>\n                </div>\n\n                <ng-template #otherField_2>\n                  <div *ngIf=\"field.hasOwnProperty('type') && field['type'] == 'DATE'; else inputField\">\n                    <clr-date-container>\n                        <label>{{field['title']}}</label>\n                        <input type=\"date\" clrDate [formControlName]=\"field['name']\">\n                    </clr-date-container>\n                  </div>\n\n                  <ng-template #inputField>\n                    <clr-input-container>\n                      <label>{{field['title']}}</label>\n                      <input clrInput [formControlName]=\"field['name']\"/>\n                    </clr-input-container>\n                  </ng-template>\n                </ng-template>\n\n              </ng-template>\n          </div>\n\n          <button class=\"btn btn-primary\" type=\"submit\" (click)=\"submitEditForm()\" style=\"margin-top: 20px;\">Сохранить</button>\n          <button class=\"btn\" type=\"button\" (click)=\"setMode(modes.List)\" style=\"margin-top: 20px;\">Закрыть</button>\n    </form>\n\n</div>\n\n<div *ngIf=\"mode === modes.List\">\n  <button  *ngIf=\"isAddRowEnabled\" type=\"button\" class=\"btn btn-icon\" aria-label=\"add\"  (click)=\"addRow()\">\n    <clr-icon shape=\"add\"></clr-icon>\n  </button>\n\n  <div *ngIf=\"isSearchEnabled\">\n    <header class=\"header header-2\">\n        <form class=\"search\" [formGroup]=\"searchForm\">\n          <label for=\"search_input\"></label>\n          <input class=\"search-input\" id=\"search_input\" type=\"text\" placeholder=\"Поиск по...\" formControlName=\"search\">\n        </form>\n    </header>\n  </div>\n\n  <clr-datagrid class=\"full-screen nowrap\" [clrDgLoading]=\"loadingList\">\n\n    <clr-dg-column *ngFor=\"let field of fields\">\n      {{field.title}}\n    </clr-dg-column>\n    <clr-dg-column *ngIf=\"isDeleteEnabled\" class=\"service-column\">Удалить</clr-dg-column>\n    <clr-dg-column *ngIf=\"isEditRowEnabled\" class=\"service-column\">Изменить</clr-dg-column>\n    <clr-dg-column *ngIf=\"isShowDetailsEnabled\" class=\"service-column\">Детали</clr-dg-column>\n\n    <clr-dg-row *ngFor=\"let item of dataList\">\n      <clr-dg-cell class=\"left\" *ngFor=\"let field of fields\">\n        {{getTableCellValue(item, field)}}\n      </clr-dg-cell>\n      <clr-dg-cell *ngIf=\"isDeleteEnabled\" class=\"left service-column link\" (click)=\"deleteRow(item)\"><clr-icon shape=\"trash\" role=\"none\"></clr-icon></clr-dg-cell>\n      <clr-dg-cell *ngIf=\"isEditRowEnabled\" class=\"left service-column link\" (click)=\"editRow(item)\"><clr-icon shape=\"pencil\" role=\"none\"></clr-icon></clr-dg-cell>\n      <clr-dg-cell *ngIf=\"isShowDetailsEnabled\" class=\"left service-column link\" (click)=\"showDetailsRow(item)\"><clr-icon shape=\"info-standard\" role=\"none\"></clr-icon></clr-dg-cell>\n    </clr-dg-row>\n\n      <clr-dg-footer>\n          <app-grid-paging (pagingFormChanges)=\"pagingFormChanges($event)\" [totalItems]=\"totalItems\"></app-grid-paging>\n      </clr-dg-footer>\n\n  </clr-datagrid>\n</div>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/grid-paging/grid-paging.component.html":
    /*!****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/grid-paging/grid-paging.component.html ***!
      \****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsGridPagingGridPagingComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<form [formGroup]=\"pagingForm\">\n  <div class=\"pagination\">\n    <div class=\"pagination-size ng-star-inserted\">\n      <span>Строк на странице</span>\n      <div class=\"clr-select-wrapper\">\n        <label>\n          <select class=\"clr-page-size-select\" formControlName=\"size\">\n            <option *ngFor=\"let item_size of sizes\" [ngValue]=\"item_size\">\n              {{ item_size }}\n            </option>\n          </select>\n        </label>\n      </div>\n    </div>\n\n    <div class=\"pagination-description ng-star-inserted\">\n      {{firstItem}} - {{lastItem}} из {{totalItems}}\n    </div>\n\n    <div class=\"pagination-list ng-star-inserted\">\n      <button type=\"button\" class=\"pagination-first\" (click)=\"setFirstPage()\" [disabled]=\"firstItem<=1\">\n        <clr-icon shape=\"step-forward-2 down\" role=\"none\"></clr-icon>\n      </button>\n      <button type=\"button\" class=\"pagination-previous\" (click)=\"setPrevPage()\" [disabled]=\"firstItem<=1\">\n        <clr-icon shape=\"angle left\" role=\"none\"></clr-icon>\n      </button>\n      <label>\n        <input type=\"text\" class=\"pagination-current clr-input ng-star-inserted\" size=\"{{sizeCurrentPageField}}\"\n               formControlName=\"page\">\n      </label>\n      <span aria-label=\"Total Pages\">&nbsp;{{totalPages}}</span>\n      <button type=\"button\" class=\"pagination-next\" (click)=\"setNextPage()\" [disabled]=\"pagingForm.value['page'] >= totalPages\">\n        <clr-icon shape=\"angle right\" role=\"none\"></clr-icon>\n      </button>\n      <button type=\"button\" class=\"pagination-last\" (click)=\"setLastPage()\" [disabled]=\"pagingForm.value['page'] >= totalPages\">\n        <clr-icon shape=\"step-forward-2 up\" role=\"none\"></clr-icon>\n      </button>\n    </div>\n\n  </div>\n</form>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/menu/menu.component.html":
    /*!**************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/menu/menu.component.html ***!
      \**************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedComponentsMenuMenuComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<clr-main-container class=\"main-container\">\n\n  <clr-header class=\"header header-6\">\n    <div class=\"branding\">\n<!--        <img src=\"icon-a.png\" alt=\"ico\" />-->\n        <span class=\"title\">Shop</span>\n    </div>\n\n    <div class=\"header-nav\">\n      <a routerLink=\"/dirs\" routerLinkActive=\"active\" class=\"nav-link\">\n        <span class=\"nav-text\">Справочники</span>\n      </a>\n      <a routerLink=\"/docs\" routerLinkActive=\"active\" class=\"nav-link\">\n        <span class=\"nav-text\">Документы</span>\n      </a>\n    </div>\n\n    <div class=\"settings\" *ngIf=\"isLoggedIn()\">\n      <div class=\"header-nav\">\n        <span class=\"nav-text nav-link\">({{getUserName()}}) </span>\n        <a routerLink=\"/logout\" routerLinkActive=\"active\" class=\"nav-link\">\n          <span class=\"nav-text\">Выход</span>\n        </a>\n      </div>\n    </div>\n  </clr-header>\n\n  <nav class=\"subnav\" *ngIf=\"isParentUrl('/dirs')\">\n    <ul class=\"nav\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dirs/measures\" routerLinkActive=\"active\">Единицы измерения</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dirs/products\" routerLinkActive=\"active\">Товары</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dirs/shops\" routerLinkActive=\"active\">Магазины</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/dirs/doc_types\" routerLinkActive=\"active\">Типы документов</a>\n      </li>\n    </ul>\n  </nav>\n\n  <nav class=\"subnav\" *ngIf=\"isParentUrl('/docs')\">\n    <ul class=\"nav\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/docs/orm\" routerLinkActive=\"active\">Документы (ORM)</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/docs/raw\" routerLinkActive=\"active\">Документы (RAW)</a>\n      </li>\n    </ul>\n  </nav>\n\n    <div class=\"content-container\">\n      <main class=\"content-area\">\n        <router-outlet></router-outlet>\n      </main>\n    </div>\n\n</clr-main-container>\n";
      /***/
    },

    /***/
    "./src/app/app-routing.module.ts":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _components_auth_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./components/auth/login/login.component */
      "./src/app/components/auth/login/login.component.ts");
      /* harmony import */


      var _components_auth_logout_logout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./components/auth/logout/logout.component */
      "./src/app/components/auth/logout/logout.component.ts");
      /* harmony import */


      var _components_measures_measures_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./components/measures/measures.component */
      "./src/app/components/measures/measures.component.ts");
      /* harmony import */


      var _shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./shared/services/auth.guard */
      "./src/app/shared/services/auth.guard.ts");
      /* harmony import */


      var _components_products_products_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./components/products/products.component */
      "./src/app/components/products/products.component.ts");
      /* harmony import */


      var _components_shops_shops_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./components/shops/shops.component */
      "./src/app/components/shops/shops.component.ts");
      /* harmony import */


      var _components_doc_types_doc_types_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./components/doc-types/doc-types.component */
      "./src/app/components/doc-types/doc-types.component.ts");
      /* harmony import */


      var _components_docs_orm_docs_orm_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./components/docs-orm/docs-orm.component */
      "./src/app/components/docs-orm/docs-orm.component.ts");
      /* harmony import */


      var _components_docs_raw_sql_docs_raw_sql_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./components/docs-raw-sql/docs-raw-sql.component */
      "./src/app/components/docs-raw-sql/docs-raw-sql.component.ts");
      /* harmony import */


      var _components_docs_details_docs_details_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./components/docs-details/docs-details.component */
      "./src/app/components/docs-details/docs-details.component.ts");

      var DEFAULT_REDIRECT = 'docs/orm';
      var routes = [{
        path: '',
        redirectTo: DEFAULT_REDIRECT,
        pathMatch: 'full',
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'dirs',
        redirectTo: 'dirs/measures',
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'dirs/measures',
        component: _components_measures_measures_component__WEBPACK_IMPORTED_MODULE_5__["MeasuresComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'dirs/products',
        component: _components_products_products_component__WEBPACK_IMPORTED_MODULE_7__["ProductsComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'dirs/shops',
        component: _components_shops_shops_component__WEBPACK_IMPORTED_MODULE_8__["ShopsComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'dirs/doc_types',
        component: _components_doc_types_doc_types_component__WEBPACK_IMPORTED_MODULE_9__["DocTypesComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'docs',
        redirectTo: 'docs/input',
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'docs/orm',
        component: _components_docs_orm_docs_orm_component__WEBPACK_IMPORTED_MODULE_10__["DocsOrmComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'docs/orm/details/:doc_id',
        component: _components_docs_details_docs_details_component__WEBPACK_IMPORTED_MODULE_12__["DocsDetailsComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'docs/raw',
        component: _components_docs_raw_sql_docs_raw_sql_component__WEBPACK_IMPORTED_MODULE_11__["DocsRawSqlComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: 'login',
        component: _components_auth_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
      }, {
        path: 'logout',
        component: _components_auth_logout_logout_component__WEBPACK_IMPORTED_MODULE_4__["LogoutComponent"],
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }, {
        path: '**',
        redirectTo: DEFAULT_REDIRECT,
        canActivate: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]],
        canActivateChild: [_shared_services_auth_guard__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]]
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AppRoutingModule);
      /***/
    },

    /***/
    "./src/app/app.component.ts":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AppComponent = function AppComponent() {
        _classCallCheck(this, AppComponent);
      };

      AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: '<app-menu></app-menu>'
      })], AppComponent);
      /***/
    },

    /***/
    "./src/app/app.module.ts":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: initFactory, AppModule */

    /***/
    function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "initFactory", function () {
        return initFactory;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/locales/fr */
      "./node_modules/@angular/common/locales/fr.js");
      /* harmony import */


      var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./app.component */
      "./src/app/app.component.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _shared_services_init_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./shared/services/init.service */
      "./src/app/shared/services/init.service.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _shared_services_csrf_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./shared/services/csrf.service */
      "./src/app/shared/services/csrf.service.ts");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./app-routing.module */
      "./src/app/app-routing.module.ts");
      /* harmony import */


      var _components_auth__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./components/auth */
      "./src/app/components/auth/index.ts");
      /* harmony import */


      var _clr_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @clr/angular */
      "./node_modules/@clr/angular/__ivy_ngcc__/fesm2015/clr-angular.js");
      /* harmony import */


      var _shared_components_menu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./shared/components/menu */
      "./src/app/shared/components/menu/index.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! @angular/platform-browser/animations */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
      /* harmony import */


      var _components_components_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./components/components.module */
      "./src/app/components/components.module.ts");

      Object(_angular_common__WEBPACK_IMPORTED_MODULE_3__["registerLocaleData"])(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_4___default.a);

      function initFactory(service) {
        return function () {
          return service.init();
        };
      }

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"], _shared_components_menu__WEBPACK_IMPORTED_MODULE_13__["MenuComponent"]],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], // CSRF 1 step
        _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientXsrfModule"].withOptions({
          cookieName: 'csrftoken',
          headerName: 'x-csrftoken'
        }), _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_15__["BrowserAnimationsModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_12__["ClarityModule"], // App modules
        _components_auth__WEBPACK_IMPORTED_MODULE_11__["AuthModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"].forRoot(), _components_components_module__WEBPACK_IMPORTED_MODULE_16__["ComponentsModule"]],
        providers: [_shared_services_init_service__WEBPACK_IMPORTED_MODULE_7__["InitService"], {
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["APP_INITIALIZER"],
          useFactory: initFactory,
          deps: [_shared_services_init_service__WEBPACK_IMPORTED_MODULE_7__["InitService"]],
          multi: true
        }, // CSRF 2 step
        {
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
          useClass: _shared_services_csrf_service__WEBPACK_IMPORTED_MODULE_9__["HttpXsrfInterceptor"],
          multi: true
        }, _angular_common__WEBPACK_IMPORTED_MODULE_3__["DatePipe"], {
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["LOCALE_ID"],
          useValue: 'fr'
        }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
      })], AppModule);
      /***/
    },

    /***/
    "./src/app/components/auth/auth.module.ts":
    /*!************************************************!*\
      !*** ./src/app/components/auth/auth.module.ts ***!
      \************************************************/

    /*! exports provided: AuthModule */

    /***/
    function srcAppComponentsAuthAuthModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthModule", function () {
        return AuthModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./login/login.component */
      "./src/app/components/auth/login/login.component.ts");
      /* harmony import */


      var _clr_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @clr/angular */
      "./node_modules/@clr/angular/__ivy_ngcc__/fesm2015/clr-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _logout_logout_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./logout/logout.component */
      "./src/app/components/auth/logout/logout.component.ts");

      var AuthModule = function AuthModule() {
        _classCallCheck(this, AuthModule);
      };

      AuthModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrCommonFormsModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrPasswordModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrSelectModule"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"]],
        declarations: [_login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"], _logout_logout_component__WEBPACK_IMPORTED_MODULE_6__["LogoutComponent"]],
        providers: []
      })], AuthModule);
      /***/
    },

    /***/
    "./src/app/components/auth/index.ts":
    /*!******************************************!*\
      !*** ./src/app/components/auth/index.ts ***!
      \******************************************/

    /*! exports provided: AuthModule */

    /***/
    function srcAppComponentsAuthIndexTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _auth_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./auth.module */
      "./src/app/components/auth/auth.module.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "AuthModule", function () {
        return _auth_module__WEBPACK_IMPORTED_MODULE_0__["AuthModule"];
      });
      /***/

    },

    /***/
    "./src/app/components/auth/login/login.component.ts":
    /*!**********************************************************!*\
      !*** ./src/app/components/auth/login/login.component.ts ***!
      \**********************************************************/

    /*! exports provided: LoginComponent */

    /***/
    function srcAppComponentsAuthLoginLoginComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginComponent", function () {
        return LoginComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../shared/services/auth.service */
      "./src/app/shared/services/auth.service.ts");
      /* harmony import */


      var _shared_services_api_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../shared/services/api.service */
      "./src/app/shared/services/api.service.ts");
      /* harmony import */


      var _shared_functions_localstorage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../shared/functions/localstorage */
      "./src/app/shared/functions/localstorage.ts");

      var LoginComponent = /*#__PURE__*/function () {
        function LoginComponent(fb, authService, api, router) {
          _classCallCheck(this, LoginComponent);

          this.fb = fb;
          this.authService = authService;
          this.api = api;
          this.router = router;
          this.loginSubs = null;
          this.isError = false;
        }

        _createClass(LoginComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.form = this.fb.group({
              username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this._loginUnsubscribe();
          } // признак корректности данных

        }, {
          key: "onSubmit",
          value: function onSubmit() {
            if (this.form.valid) {
              this._clear();

              this.loginSubscribe(this.form.value);
            }
          } // подписка на логин

        }, {
          key: "loginSubscribe",
          value: function loginSubscribe(user) {
            var _this = this;

            this._loginUnsubscribe();

            var params = {
              username: user.username,
              password: user.password
            };
            this.loginSubs = this.api.apiLogin(params).subscribe(function (res) {
              if (res.result && res.result.auth === true) {
                Object(_shared_functions_localstorage__WEBPACK_IMPORTED_MODULE_6__["setUserInfo"])(res.result);

                _this.loginOk();
              }
            }, function () {
              _this.isError = true;
            });
          }
        }, {
          key: "loginOk",
          value: function loginOk() {
            this.authService.setLoggedIn(true); // noinspection JSIgnoredPromiseFromCall

            this.router.navigateByUrl('/');
          }
        }, {
          key: "_loginUnsubscribe",
          value: function _loginUnsubscribe() {
            if (this.loginSubs !== null) {
              // отписка логин
              this.loginSubs.unsubscribe();
            }
          } // очистка

        }, {
          key: "_clear",
          value: function _clear() {
            // очистка аутентификации
            this.authService.clear();
          }
        }, {
          key: "isWrongData",
          get: function get() {
            return this.isError;
          }
        }]);

        return LoginComponent;
      }();

      LoginComponent.ctorParameters = function () {
        return [{
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _shared_services_api_service__WEBPACK_IMPORTED_MODULE_5__["ApiService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      LoginComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/auth/login/login.component.html"))["default"]
      })], LoginComponent);
      /***/
    },

    /***/
    "./src/app/components/auth/logout/logout.component.css":
    /*!*************************************************************!*\
      !*** ./src/app/components/auth/logout/logout.component.css ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsAuthLogoutLogoutComponentCss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYXV0aC9sb2dvdXQvbG9nb3V0LmNvbXBvbmVudC5jc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/components/auth/logout/logout.component.ts":
    /*!************************************************************!*\
      !*** ./src/app/components/auth/logout/logout.component.ts ***!
      \************************************************************/

    /*! exports provided: LogoutComponent */

    /***/
    function srcAppComponentsAuthLogoutLogoutComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LogoutComponent", function () {
        return LogoutComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../shared/services/api.service */
      "./src/app/shared/services/api.service.ts");
      /* harmony import */


      var _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../shared/services/auth.service */
      "./src/app/shared/services/auth.service.ts");

      var LogoutComponent = /*#__PURE__*/function () {
        function LogoutComponent(api, authService, router) {
          _classCallCheck(this, LogoutComponent);

          this.api = api;
          this.authService = authService;
          this.router = router;
          this.title = 'Fraud Detect Platform';
          this.apiLogoutSubs = null;
        }

        _createClass(LogoutComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.logout();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.unsubscribe();
          }
        }, {
          key: "unsubscribe",
          value: function unsubscribe() {
            if (this.apiLogoutSubs !== null) {
              this.apiLogoutSubs.unsubscribe();
            }
          } // выход

        }, {
          key: "logout",
          value: function logout() {
            var _this2 = this;

            this.unsubscribe();
            this.apiLogoutSubs = this.api.apiLogout().subscribe(function (res) {
              if (res) {
                _this2.authService.clear(); // noinspection JSIgnoredPromiseFromCall


                _this2.router.navigateByUrl('/');
              }
            });
          }
        }]);

        return LogoutComponent;
      }();

      LogoutComponent.ctorParameters = function () {
        return [{
          type: _shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]
        }, {
          type: _shared_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      LogoutComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-logout',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./logout.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/auth/logout/logout.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./logout.component.css */
        "./src/app/components/auth/logout/logout.component.css"))["default"]]
      })], LogoutComponent);
      /***/
    },

    /***/
    "./src/app/components/components.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/components/components.module.ts ***!
      \*************************************************/

    /*! exports provided: ComponentsModule */

    /***/
    function srcAppComponentsComponentsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ComponentsModule", function () {
        return ComponentsModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _measures_measures_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./measures/measures.component */
      "./src/app/components/measures/measures.component.ts");
      /* harmony import */


      var _products_products_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./products/products.component */
      "./src/app/components/products/products.component.ts");
      /* harmony import */


      var _shops_shops_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./shops/shops.component */
      "./src/app/components/shops/shops.component.ts");
      /* harmony import */


      var _doc_types_doc_types_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./doc-types/doc-types.component */
      "./src/app/components/doc-types/doc-types.component.ts");
      /* harmony import */


      var _docs_orm_docs_orm_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./docs-orm/docs-orm.component */
      "./src/app/components/docs-orm/docs-orm.component.ts");
      /* harmony import */


      var _clr_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @clr/angular */
      "./node_modules/@clr/angular/__ivy_ngcc__/fesm2015/clr-angular.js");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _docs_raw_sql_docs_raw_sql_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./docs-raw-sql/docs-raw-sql.component */
      "./src/app/components/docs-raw-sql/docs-raw-sql.component.ts");
      /* harmony import */


      var _docs_details_docs_details_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./docs-details/docs-details.component */
      "./src/app/components/docs-details/docs-details.component.ts");

      var ComponentsModule_1;

      var ComponentsModule = ComponentsModule_1 = /*#__PURE__*/function () {
        function ComponentsModule() {
          _classCallCheck(this, ComponentsModule);
        }

        _createClass(ComponentsModule, null, [{
          key: "forRoot",
          value: function forRoot() {
            return {
              ngModule: ComponentsModule_1,
              providers: []
            };
          }
        }]);

        return ComponentsModule;
      }();

      ComponentsModule = ComponentsModule_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_clr_angular__WEBPACK_IMPORTED_MODULE_7__["ClrDatagridModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_7__["ClrCommonFormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_7__["ClrInputModule"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["CommonModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_7__["ClrIconModule"]],
        exports: [],
        declarations: [_measures_measures_component__WEBPACK_IMPORTED_MODULE_2__["MeasuresComponent"], _products_products_component__WEBPACK_IMPORTED_MODULE_3__["ProductsComponent"], _shops_shops_component__WEBPACK_IMPORTED_MODULE_4__["ShopsComponent"], _doc_types_doc_types_component__WEBPACK_IMPORTED_MODULE_5__["DocTypesComponent"], _docs_orm_docs_orm_component__WEBPACK_IMPORTED_MODULE_6__["DocsOrmComponent"], _docs_raw_sql_docs_raw_sql_component__WEBPACK_IMPORTED_MODULE_11__["DocsRawSqlComponent"], _docs_details_docs_details_component__WEBPACK_IMPORTED_MODULE_12__["DocsDetailsComponent"]],
        providers: []
      })], ComponentsModule);
      /***/
    },

    /***/
    "./src/app/components/doc-types/doc-types.component.scss":
    /*!***************************************************************!*\
      !*** ./src/app/components/doc-types/doc-types.component.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsDocTypesDocTypesComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZG9jLXR5cGVzL2RvYy10eXBlcy5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/components/doc-types/doc-types.component.ts":
    /*!*************************************************************!*\
      !*** ./src/app/components/doc-types/doc-types.component.ts ***!
      \*************************************************************/

    /*! exports provided: DocTypesComponent */

    /***/
    function srcAppComponentsDocTypesDocTypesComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DocTypesComponent", function () {
        return DocTypesComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");

      var DocTypesComponent = function DocTypesComponent() {
        _classCallCheck(this, DocTypesComponent);

        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/doc_type/';
        this.fields = [{
          name: 'id',
          title: 'Id',
          "var": 'id',
          readOnly: true
        }, {
          name: 'name',
          title: 'Наименование',
          "var": 'name'
        }];
      };

      DocTypesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-doc-types',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./doc-types.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/doc-types/doc-types.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./doc-types.component.scss */
        "./src/app/components/doc-types/doc-types.component.scss"))["default"]]
      })], DocTypesComponent);
      /***/
    },

    /***/
    "./src/app/components/docs-details/docs-details.component.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/components/docs-details/docs-details.component.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsDocsDetailsDocsDetailsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZG9jcy1kZXRhaWxzL2RvY3MtZGV0YWlscy5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/components/docs-details/docs-details.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/components/docs-details/docs-details.component.ts ***!
      \*******************************************************************/

    /*! exports provided: DocsDetailsComponent */

    /***/
    function srcAppComponentsDocsDetailsDocsDetailsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DocsDetailsComponent", function () {
        return DocsDetailsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _shared_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../shared/functions/unsubscribe */
      "./src/app/shared/functions/unsubscribe.ts");

      var DocsDetailsComponent = /*#__PURE__*/function () {
        function DocsDetailsComponent(_activateRoute, router) {
          _classCallCheck(this, DocsDetailsComponent);

          this._activateRoute = _activateRoute;
          this.router = router;
          this._subscriptionRoute = null;
          this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/document_details/';
          this.fields = [// {name: 'id', title: 'Id', readOnly: true},
          {
            name: 'product_id',
            title: 'Наименование товара'
          }, {
            name: 'amount',
            title: 'Количество'
          }];
          this.editFormSelects = {
            product_id: {
              source: 'product',
              key: 'id',
              value: 'name',
              apiUrl: _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/product_list/'
            }
          };
          this.eventCallBacks = {
            beforeCreateRequest: this.addDocId,
            beforeUpdateRequest: this.addDocId,
            beforeListRequest: this.addDocId
          };
        }

        _createClass(DocsDetailsComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            localStorage.removeItem('DOC_ID');

            this._routeParamsSubscribe();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            [this._subscriptionRoute].map(function (subs) {
              Object(_shared_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_4__["unSubscribe"])(subs);
            });
          }
        }, {
          key: "_routeParamsSubscribe",
          value: function _routeParamsSubscribe() {
            var _this3 = this;

            // параметры из роута
            this._subscriptionRoute = this._activateRoute.params.subscribe(function (res) {
              _this3.docID = res.doc_id;
              localStorage.setItem('DOC_ID', String(res.doc_id));
            });
          }
        }, {
          key: "returnToDocList",
          value: function returnToDocList() {
            this.router.navigate([_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/document/']);
          }
        }, {
          key: "addDocId",
          value: function addDocId(data, scope) {
            data.document_id = localStorage.getItem('DOC_ID');
            return data;
          }
        }]);

        return DocsDetailsComponent;
      }();

      DocsDetailsComponent.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      DocsDetailsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-docs-details',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./docs-details.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-details/docs-details.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./docs-details.component.scss */
        "./src/app/components/docs-details/docs-details.component.scss"))["default"]]
      })], DocsDetailsComponent);
      /***/
    },

    /***/
    "./src/app/components/docs-orm/docs-orm.component.scss":
    /*!*************************************************************!*\
      !*** ./src/app/components/docs-orm/docs-orm.component.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsDocsOrmDocsOrmComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZG9jcy1vcm0vZG9jcy1vcm0uY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/components/docs-orm/docs-orm.component.ts":
    /*!***********************************************************!*\
      !*** ./src/app/components/docs-orm/docs-orm.component.ts ***!
      \***********************************************************/

    /*! exports provided: DocsOrmComponent */

    /***/
    function srcAppComponentsDocsOrmDocsOrmComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DocsOrmComponent", function () {
        return DocsOrmComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _shared_functions_localstorage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../shared/functions/localstorage */
      "./src/app/shared/functions/localstorage.ts");

      var DocsOrmComponent = /*#__PURE__*/function () {
        function DocsOrmComponent() {
          _classCallCheck(this, DocsOrmComponent);

          this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/document/';
          this.isShowDetailsEnabled = true;
          this.detailsComponentParams = {
            url: '/docs/orm/details',
            key: 'id'
          };
          this.fields = [{
            name: 'id',
            title: 'Id',
            readOnly: true
          }, {
            name: 'number',
            title: 'Номер'
          }, {
            name: 'date',
            title: 'Дата',
            type: 'DATE'
          }, {
            name: 'shop_id',
            title: 'Магазин'
          }, {
            name: 'type_id',
            title: 'Тип документа'
          }, {
            name: 'positions',
            title: 'Кол-во позиций',
            readOnly: true
          }, {
            name: 'manager_id',
            title: 'Менеджер',
            display: this.getManagerName
          }];
          this.editFormSelects = {
            shop_id: {
              source: 'shop',
              key: 'id',
              value: 'name',
              apiUrl: _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/shop_list/'
            },
            type_id: {
              source: 'type',
              key: 'id',
              value: 'name',
              apiUrl: _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/doc_type_list/'
            }
          };
          this.eventCallBacks = {
            beforeCreateRequest: this.addManagerId,
            beforeUpdateRequest: this.addManagerId
          };
        }

        _createClass(DocsOrmComponent, [{
          key: "getManagerName",
          value: function getManagerName(field, item, scope) {
            if (scope.mode === scope.modes.AddRow) {
              return Object(_shared_functions_localstorage__WEBPACK_IMPORTED_MODULE_3__["getUserName"])();
            }

            if (scope.mode === scope.modes.EditRow) {
              return scope.updateFields.manager.username;
            }

            return item.manager.username;
          }
        }, {
          key: "addManagerId",
          value: function addManagerId(data) {
            data.manager_id = Object(_shared_functions_localstorage__WEBPACK_IMPORTED_MODULE_3__["getUserId"])();
            return data;
          }
        }]);

        return DocsOrmComponent;
      }();

      DocsOrmComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-docs-orm',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./docs-orm.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-orm/docs-orm.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./docs-orm.component.scss */
        "./src/app/components/docs-orm/docs-orm.component.scss"))["default"]]
      })], DocsOrmComponent);
      /***/
    },

    /***/
    "./src/app/components/docs-raw-sql/docs-raw-sql.component.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/components/docs-raw-sql/docs-raw-sql.component.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsDocsRawSqlDocsRawSqlComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZG9jcy1yYXctc3FsL2RvY3MtcmF3LXNxbC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/components/docs-raw-sql/docs-raw-sql.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/components/docs-raw-sql/docs-raw-sql.component.ts ***!
      \*******************************************************************/

    /*! exports provided: DocsRawSqlComponent */

    /***/
    function srcAppComponentsDocsRawSqlDocsRawSqlComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DocsRawSqlComponent", function () {
        return DocsRawSqlComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");

      var DocsRawSqlComponent = function DocsRawSqlComponent() {
        _classCallCheck(this, DocsRawSqlComponent);

        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/doc_raw_sql/';
        this.isAddRowEnabled = false;
        this.isEditRowEnabled = false;
        this.isDeleteEnabled = false;
        this.fields = [{
          name: 'id',
          title: 'Id',
          readOnly: true
        }, {
          name: 'number',
          title: 'Номер'
        }, {
          name: 'date',
          title: 'Дата',
          type: 'DATE'
        }, {
          name: 'shop_name',
          title: 'Магазин'
        }, {
          name: 'doctype_name',
          title: 'Тип документа'
        }, {
          name: 'manager_name',
          title: 'Менеджер'
        }];
      };

      DocsRawSqlComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-docs-raw-sql',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./docs-raw-sql.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/docs-raw-sql/docs-raw-sql.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./docs-raw-sql.component.scss */
        "./src/app/components/docs-raw-sql/docs-raw-sql.component.scss"))["default"]]
      })], DocsRawSqlComponent);
      /***/
    },

    /***/
    "./src/app/components/measures/measures.component.scss":
    /*!*************************************************************!*\
      !*** ./src/app/components/measures/measures.component.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsMeasuresMeasuresComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbWVhc3VyZXMvbWVhc3VyZXMuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/components/measures/measures.component.ts":
    /*!***********************************************************!*\
      !*** ./src/app/components/measures/measures.component.ts ***!
      \***********************************************************/

    /*! exports provided: MeasuresComponent */

    /***/
    function srcAppComponentsMeasuresMeasuresComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MeasuresComponent", function () {
        return MeasuresComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");

      var MeasuresComponent = function MeasuresComponent() {
        _classCallCheck(this, MeasuresComponent);

        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/measure/';
        this.fields = [{
          name: 'id',
          title: 'Id',
          "var": 'id',
          readOnly: true
        }, {
          name: 'name',
          title: 'Наименование',
          "var": 'name'
        }];
      };

      MeasuresComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-measures',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./measures.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/measures/measures.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./measures.component.scss */
        "./src/app/components/measures/measures.component.scss"))["default"]]
      })], MeasuresComponent);
      /***/
    },

    /***/
    "./src/app/components/products/products.component.scss":
    /*!*************************************************************!*\
      !*** ./src/app/components/products/products.component.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsProductsProductsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcHJvZHVjdHMvcHJvZHVjdHMuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/components/products/products.component.ts":
    /*!***********************************************************!*\
      !*** ./src/app/components/products/products.component.ts ***!
      \***********************************************************/

    /*! exports provided: ProductsComponent */

    /***/
    function srcAppComponentsProductsProductsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductsComponent", function () {
        return ProductsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");

      var ProductsComponent = function ProductsComponent() {
        _classCallCheck(this, ProductsComponent);

        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/product/';
        this.fields = [{
          name: 'id',
          title: 'Id',
          readOnly: true
        }, {
          name: 'name',
          title: 'Наименование'
        }, {
          name: 'measure_id',
          title: 'Единица измерения'
        }];
        this.editFormSelects = {
          measure_id: {
            source: 'measure',
            key: 'id',
            value: 'name',
            apiUrl: _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/measure_list/'
          }
        };
      };

      ProductsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-products',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./products.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/products/products.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./products.component.scss */
        "./src/app/components/products/products.component.scss"))["default"]]
      })], ProductsComponent);
      /***/
    },

    /***/
    "./src/app/components/shops/shops.component.scss":
    /*!*******************************************************!*\
      !*** ./src/app/components/shops/shops.component.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppComponentsShopsShopsComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc2hvcHMvc2hvcHMuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/components/shops/shops.component.ts":
    /*!*****************************************************!*\
      !*** ./src/app/components/shops/shops.component.ts ***!
      \*****************************************************/

    /*! exports provided: ShopsComponent */

    /***/
    function srcAppComponentsShopsShopsComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ShopsComponent", function () {
        return ShopsComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");

      var ShopsComponent = function ShopsComponent() {
        _classCallCheck(this, ShopsComponent);

        this.apiUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].host + '/api/shop/shop/';
        this.fields = [{
          name: 'id',
          title: 'Id',
          "var": 'id',
          readOnly: true
        }, {
          name: 'name',
          title: 'Наименование',
          "var": 'name'
        }];
      };

      ShopsComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-shops',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./shops.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/components/shops/shops.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./shops.component.scss */
        "./src/app/components/shops/shops.component.scss"))["default"]]
      })], ShopsComponent);
      /***/
    },

    /***/
    "./src/app/shared/components/grid-base/grid-base.component.css":
    /*!*********************************************************************!*\
      !*** ./src/app/shared/components/grid-base/grid-base.component.css ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedComponentsGridBaseGridBaseComponentCss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".search-input {\n  margin: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvZ3JpZC1iYXNlL2dyaWQtYmFzZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvZ3JpZC1iYXNlL2dyaWQtYmFzZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnNlYXJjaC1pbnB1dCB7XG4gIG1hcmdpbjogMjBweDtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/shared/components/grid-base/grid-base.component.ts":
    /*!********************************************************************!*\
      !*** ./src/app/shared/components/grid-base/grid-base.component.ts ***!
      \********************************************************************/

    /*! exports provided: getDefaultGridPageParams, GridBaseComponent */

    /***/
    function srcAppSharedComponentsGridBaseGridBaseComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "getDefaultGridPageParams", function () {
        return getDefaultGridPageParams;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GridBaseComponent", function () {
        return GridBaseComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _grid_paging_grid_paging_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../grid-paging/grid-paging.component */
      "./src/app/shared/components/grid-paging/grid-paging.component.ts");
      /* harmony import */


      var _services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/api.service */
      "./src/app/shared/services/api.service.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../functions/unsubscribe */
      "./src/app/shared/functions/unsubscribe.ts");
      /* harmony import */


      var _functions_dates__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../functions/dates */
      "./src/app/shared/functions/dates.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      function getDefaultGridPageParams() {
        return {
          page: 1,
          size: Object(_grid_paging_grid_paging_component__WEBPACK_IMPORTED_MODULE_2__["getDefaultPageSize"])(),
          firstRow: 1
        };
      }

      var GridBaseComponent = /*#__PURE__*/function () {
        function GridBaseComponent(apiService, fbEditForm, router) {
          _classCallCheck(this, GridBaseComponent);

          this.apiService = apiService;
          this.fbEditForm = fbEditForm;
          this.router = router;
          this.subsList = null;
          this.subsCreate = null;
          this.subsUpdate = null;
          this.subsDelete = null;
          this.modes = {
            List: 1,
            AddRow: 2,
            EditRow: 3
          };
          this.mode = this.modes.List; // List

          this.loadingList = true;
          this.dataList = [];
          this.totalItems = 0;
          this.isErrorCreate = false;
          this.updateFields = {};
          this.isSearchEnabled = true;
          this.isAddRowEnabled = true;
          this.isEditRowEnabled = true;
          this.isDeleteEnabled = true;
          this.isShowDetailsEnabled = false;
        }

        _createClass(GridBaseComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initEditForm();
            this.initSearchForm();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            [this.subsList, this.subsCreate, this.subsUpdate, this.subsDelete].map(function (subs) {
              Object(_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__["unSubscribe"])(subs);
            });
          }
        }, {
          key: "initEditForm",
          value: function initEditForm() {
            var groupControls = {};

            var _iterator = _createForOfIteratorHelper(this.fields),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var field = _step.value;

                if (field.hasOwnProperty('readOnly') && field['readOnly'] === true || field.hasOwnProperty('display')) {
                  groupControls[field['name']] = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]({
                    value: null,
                    disabled: true
                  }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required);
                } else {
                  groupControls[field['name']] = ['', {
                    validators: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required
                  }];
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            this.editForm = this.fbEditForm.group(Object.assign({}, groupControls));
          } // List

        }, {
          key: "pagingFormChanges",
          value: function pagingFormChanges(pageParams) {
            this.pagingParams = pageParams;
            this.loadDataList();
          }
        }, {
          key: "loadDataList",
          value: function loadDataList() {
            var _this4 = this;

            Object(_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__["unSubscribe"])(this.subsList);
            this.loadingList = true;
            var params = Object.assign({}, this.pagingParams, this.searchForm.value);
            params = this.callBackRun('beforeListRequest', params);
            this.subsList = this.apiService.List(this.apiUrl, params).subscribe(function (res) {
              if (res) {
                _this4.dataList = _this4.callBackRun('afterLoadDataList', res.results);
                _this4.totalItems = res.count;
                _this4.loadingList = false;
              }
            });
          } // EditForm

        }, {
          key: "submitEditForm",
          value: function submitEditForm() {
            if (!this.editForm.valid) {
              return;
            }

            if (this.mode === this.modes.AddRow) {
              this.create(this.editForm.value);
            }

            if (this.mode === this.modes.EditRow) {
              this.update(this.editForm.value);
            }
          } // Search

        }, {
          key: "initSearchForm",
          value: function initSearchForm() {
            this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
              search: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('')
            });
            this.onSearchFormChanges();
          }
        }, {
          key: "onSearchFormChanges",
          value: function onSearchFormChanges() {
            var _this5 = this;

            this.searchForm.valueChanges.subscribe(function () {
              if (!_this5.searchForm.value.search) {
                _this5.pagingParams = getDefaultGridPageParams();
              }

              _this5.loadDataList();
            });
          } // Create

        }, {
          key: "addRow",
          value: function addRow() {
            this.editForm.reset();
            this.setMode(this.modes.AddRow);
          } // подписка на создание записи

        }, {
          key: "create",
          value: function create(params) {
            var _this6 = this;

            Object(_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__["unSubscribe"])(this.subsCreate);
            params = this.formatParamsBeforeRequest(params);
            params = this.callBackRun('beforeCreateRequest', params);
            this.subsCreate = this.apiService.Create(this.apiUrl, params).subscribe(function (res) {
              if (res) {
                _this6.setMode(_this6.modes.List);

                _this6.isErrorCreate = false;

                _this6.loadDataList();
              }
            }, function () {
              _this6.isErrorCreate = true;
            });
          } // UpdateForm

        }, {
          key: "editRow",
          value: function editRow(item) {
            this.updateFields = Object.assign({}, item);
            var updateFields = {};
            var controls = this.editForm.controls;
            Object.keys(controls).map(function (objectKey) {
              // Date
              if (objectKey.includes('date')) {
                updateFields[objectKey] = Object(_functions_dates__WEBPACK_IMPORTED_MODULE_6__["reverseDAteFormat"])(item[objectKey]);
              } else {
                updateFields[objectKey] = item[objectKey];
              }
            });
            this.editForm.setValue(updateFields);
            this.setMode(this.modes.EditRow);
          } // подписка на обновление записи

        }, {
          key: "update",
          value: function update(params) {
            var _this7 = this;

            Object(_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__["unSubscribe"])(this.subsUpdate);
            var updateFields = Object.assign(this.updateFields, this.formatParamsBeforeRequest(params));
            updateFields = this.callBackRun('beforeUpdateRequest', updateFields); // @ts-ignore

            this.subsUpdate = this.apiService.Update(this.apiUrl, updateFields, updateFields.id).subscribe(function (res) {
              if (res) {
                _this7.setMode(_this7.modes.List);

                _this7.isErrorCreate = false;

                _this7.loadDataList();
              }
            }, function () {
              _this7.isErrorCreate = true;
            });
          }
        }, {
          key: "formatParamsBeforeRequest",
          value: function formatParamsBeforeRequest(params) {
            Object.keys(params).map(function (fieldName) {
              // Date
              if (fieldName.includes('date')) {
                params[fieldName] = Object(_functions_dates__WEBPACK_IMPORTED_MODULE_6__["reverseDAteFormat"])(params[fieldName]);
              }
            });
            return params;
          } // Delete

        }, {
          key: "deleteRow",
          value: function deleteRow(item) {
            this["delete"](item);
          } // подписка на удаление записи

        }, {
          key: "delete",
          value: function _delete(params) {
            var _this8 = this;

            Object(_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__["unSubscribe"])(this.subsDelete);
            this.subsDelete = this.apiService.Delete(this.apiUrl, params.id).subscribe(function () {
              _this8.setMode(_this8.modes.List);

              _this8.isErrorCreate = false;

              _this8.loadDataList();
            }, function () {
              _this8.isErrorCreate = true;
            });
          }
        }, {
          key: "setMode",
          value: function setMode(mode) {
            var oldMode = this.mode;
            this.mode = mode; // если включается режим редактирвания, нужно проверить импорт справочников

            if ([this.modes.AddRow, this.modes.EditRow].includes(mode)) {
              this.loadSelects();
            }

            console.log('Change mode from ' + oldMode + ' to ' + mode);
            this.callBackRun('afterModeChanged', {
              oldMode: oldMode,
              mode: mode,
              scope: this
            });
          } // Selects

        }, {
          key: "loadSelects",
          value: function loadSelects() {
            if (!this.editFormSelects) {
              return;
            }

            for (var _i = 0, _Object$keys = Object.keys(this.editFormSelects); _i < _Object$keys.length; _i++) {
              var selectKey = _Object$keys[_i];
              this.loadSelectOptions(selectKey);
            }
          }
        }, {
          key: "loadSelectOptions",
          value: function loadSelectOptions(selectKey) {
            var select = this.editFormSelects[selectKey];

            if (select.dataList) {
              return;
            }

            Object(_functions_unsubscribe__WEBPACK_IMPORTED_MODULE_5__["unSubscribe"])(select.subs);
            select.subs = this.apiService.List(select.apiUrl, null).subscribe(function (res) {
              if (res) {
                if (res.results) {
                  select.dataList = res.results;
                } else {
                  select.dataList = res;
                }
              }
            });
          }
        }, {
          key: "getSelectOptions",
          value: function getSelectOptions(nameField) {
            var select = this.editFormSelects[nameField];
            return select.dataList || [];
          }
        }, {
          key: "callBackRun",
          value: function callBackRun(callBackName, data) {
            if (this.eventCallBacks && this.eventCallBacks.hasOwnProperty(callBackName)) {
              return this.eventCallBacks[callBackName](data, this);
            }

            return data;
          }
        }, {
          key: "getTableCellValue",
          value: function getTableCellValue(item, field) {
            // Select
            if (this.editFormSelects && this.editFormSelects.hasOwnProperty(field.name)) {
              var select = this.editFormSelects[field.name];
              return item[select['source']][select['value']];
            } // View only property


            if (field.hasOwnProperty('display')) {
              return field['display'](field, item, this);
            }

            return item[field.name];
          } // Details

        }, {
          key: "showDetailsRow",
          value: function showDetailsRow(item) {
            this.router.navigate([this.detailsComponentParams['url'], item[this.detailsComponentParams['key']]]);
          }
        }]);

        return GridBaseComponent;
      }();

      GridBaseComponent.ctorParameters = function () {
        return [{
          type: _services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }];
      };

      GridBaseComponent.propDecorators = {
        apiUrl: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        fields: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        eventCallBacks: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        editFormSelects: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        isSearchEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        isAddRowEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        isEditRowEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        isDeleteEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        isShowDetailsEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        detailsComponentParams: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      GridBaseComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-grid-base',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./grid-base.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/grid-base/grid-base.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./grid-base.component.css */
        "./src/app/shared/components/grid-base/grid-base.component.css"))["default"]]
      })], GridBaseComponent);
      /***/
    },

    /***/
    "./src/app/shared/components/grid-paging/grid-paging.component.css":
    /*!*************************************************************************!*\
      !*** ./src/app/shared/components/grid-paging/grid-paging.component.css ***!
      \*************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedComponentsGridPagingGridPagingComponentCss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2dyaWQtcGFnaW5nL2dyaWQtcGFnaW5nLmNvbXBvbmVudC5jc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/components/grid-paging/grid-paging.component.ts":
    /*!************************************************************************!*\
      !*** ./src/app/shared/components/grid-paging/grid-paging.component.ts ***!
      \************************************************************************/

    /*! exports provided: Sizes, getDefaultPageSize, GridPagingComponent */

    /***/
    function srcAppSharedComponentsGridPagingGridPagingComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Sizes", function () {
        return Sizes;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "getDefaultPageSize", function () {
        return getDefaultPageSize;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GridPagingComponent", function () {
        return GridPagingComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _functions_localstorage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../functions/localstorage */
      "./src/app/shared/functions/localstorage.ts");

      var Sizes = [10, 20, 50, 100];

      function getDefaultPageSize() {
        return Number(Object(_functions_localstorage__WEBPACK_IMPORTED_MODULE_3__["getGridPageSize"])() || Sizes[2]);
      }

      var GridPagingComponent = /*#__PURE__*/function () {
        function GridPagingComponent() {
          _classCallCheck(this, GridPagingComponent);

          this.pagingFormChanges = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.sizes = Sizes;
        }

        _createClass(GridPagingComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.pagingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              page: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](1),
              size: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](getDefaultPageSize())
            });
            this.onPagingFormChanges();
          }
        }, {
          key: "setFirstPage",
          value: function setFirstPage() {
            return this.pagingForm.patchValue({
              page: 1
            });
          }
        }, {
          key: "setLastPage",
          value: function setLastPage() {
            return this.pagingForm.patchValue({
              page: this.totalPages
            });
          }
        }, {
          key: "setPrevPage",
          value: function setPrevPage() {
            return this.pagingForm.patchValue({
              page: parseInt(this.pagingForm.value.page, 10) - 1
            });
          }
        }, {
          key: "setNextPage",
          value: function setNextPage() {
            return this.pagingForm.patchValue({
              page: parseInt(this.pagingForm.value.page, 10) + 1
            });
          }
        }, {
          key: "callPagingFormChanges",
          value: function callPagingFormChanges() {
            var params = Object.assign({
              firstRow: this.firstItem
            }, this.pagingForm.value);
            this.pagingFormChanges.emit(params);
          }
        }, {
          key: "onPagingFormChanges",
          value: function onPagingFormChanges() {
            var _this9 = this;

            this.callPagingFormChanges();
            this.pagingForm.valueChanges.subscribe(function () {
              if (!Number(_this9.pagingForm.value.page) || _this9.pagingForm.value.page < 1) {
                _this9.setFirstPage();

                return;
              }

              if (_this9.pagingForm.value.page > _this9.totalPages) {
                _this9.setLastPage();

                return;
              }

              Object(_functions_localstorage__WEBPACK_IMPORTED_MODULE_3__["setGridPageSize"])(_this9.pagingForm.value.size);

              _this9.callPagingFormChanges();
            });
          }
        }, {
          key: "firstItem",
          get: function get() {
            return (this.pagingForm.value.page - 1) * this.pagingForm.value.size + 1;
          }
        }, {
          key: "lastItem",
          get: function get() {
            return Math.min(this.firstItem + this.pagingForm.value.size - 1, this.totalItems);
          }
        }, {
          key: "sizeCurrentPageField",
          get: function get() {
            return String(this.totalItems).length;
          }
        }, {
          key: "totalPages",
          get: function get() {
            return Math.ceil(this.totalItems / this.pagingForm.value.size);
          }
        }]);

        return GridPagingComponent;
      }();

      GridPagingComponent.propDecorators = {
        totalItems: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        pagingFormChanges: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      GridPagingComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-grid-paging',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./grid-paging.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/grid-paging/grid-paging.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./grid-paging.component.css */
        "./src/app/shared/components/grid-paging/grid-paging.component.css"))["default"]]
      })], GridPagingComponent);
      /***/
    },

    /***/
    "./src/app/shared/components/menu/index.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/components/menu/index.ts ***!
      \*************************************************/

    /*! exports provided: MenuComponent */

    /***/
    function srcAppSharedComponentsMenuIndexTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _menu_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! ./menu.component */
      "./src/app/shared/components/menu/menu.component.ts");
      /* harmony reexport (safe) */


      __webpack_require__.d(__webpack_exports__, "MenuComponent", function () {
        return _menu_component__WEBPACK_IMPORTED_MODULE_0__["MenuComponent"];
      });
      /***/

    },

    /***/
    "./src/app/shared/components/menu/menu.component.scss":
    /*!************************************************************!*\
      !*** ./src/app/shared/components/menu/menu.component.scss ***!
      \************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedComponentsMenuMenuComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".sidenav {\n  width: auto;\n  min-width: 0;\n}\n.sidenav .nav-group {\n  padding-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvbWVudS9tZW51LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFDRjtBQUFFO0VBQ0Usa0JBQUE7QUFFSiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL21lbnUvbWVudS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zaWRlbmF2IHtcbiAgd2lkdGg6IGF1dG87XG4gIG1pbi13aWR0aDogMDtcbiAgLm5hdi1ncm91cCB7XG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xuICB9XG59XG5cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/shared/components/menu/menu.component.ts":
    /*!**********************************************************!*\
      !*** ./src/app/shared/components/menu/menu.component.ts ***!
      \**********************************************************/

    /*! exports provided: MenuComponent */

    /***/
    function srcAppSharedComponentsMenuMenuComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MenuComponent", function () {
        return MenuComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../services/auth.service */
      "./src/app/shared/services/auth.service.ts");
      /* harmony import */


      var _functions_localstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../functions/localstorage */
      "./src/app/shared/functions/localstorage.ts");

      var MenuComponent = /*#__PURE__*/function () {
        function MenuComponent(router, authService) {
          _classCallCheck(this, MenuComponent);

          this.router = router;
          this.authService = authService;
          this.getUserName = _functions_localstorage__WEBPACK_IMPORTED_MODULE_4__["getUserName"];
        }

        _createClass(MenuComponent, [{
          key: "isLoggedIn",
          value: function isLoggedIn() {
            return this.authService.isLoggedIn;
          }
        }, {
          key: "isParentUrl",
          value: function isParentUrl(url) {
            return this.router.routerState.snapshot.url.startsWith(url);
          }
        }]);

        return MenuComponent;
      }();

      MenuComponent.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }];
      };

      MenuComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./menu.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/components/menu/menu.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./menu.component.scss */
        "./src/app/shared/components/menu/menu.component.scss"))["default"]]
      })], MenuComponent);
      /***/
    },

    /***/
    "./src/app/shared/functions/dates.ts":
    /*!*******************************************!*\
      !*** ./src/app/shared/functions/dates.ts ***!
      \*******************************************/

    /*! exports provided: reverseDAteFormat */

    /***/
    function srcAppSharedFunctionsDatesTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "reverseDAteFormat", function () {
        return reverseDAteFormat;
      });

      function reverseDAteFormat(date) {
        var sep = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '/';
        return date.replace('-', sep).split(sep).reverse().join('-');
      }
      /***/

    },

    /***/
    "./src/app/shared/functions/localstorage.ts":
    /*!**************************************************!*\
      !*** ./src/app/shared/functions/localstorage.ts ***!
      \**************************************************/

    /*! exports provided: clearLocalStorage, setUserInfo, getUserId, getUserName, setGridPageSize, getGridPageSize */

    /***/
    function srcAppSharedFunctionsLocalstorageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "clearLocalStorage", function () {
        return clearLocalStorage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "setUserInfo", function () {
        return setUserInfo;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "getUserId", function () {
        return getUserId;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "getUserName", function () {
        return getUserName;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "setGridPageSize", function () {
        return setGridPageSize;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "getGridPageSize", function () {
        return getGridPageSize;
      });

      var CURRENT_USER_ID = 'currentUserId';
      var CURRENT_USER_NAME = 'currentUserName';
      var GRID_PAGE_SIZE = 'gridPageSize';

      function clearLocalStorage() {
        localStorage.removeItem(CURRENT_USER_ID);
        localStorage.removeItem(CURRENT_USER_NAME);
      } // UserInfo


      function setUserInfo(obj) {
        localStorage.setItem(CURRENT_USER_ID, String(obj.user_id));
        localStorage.setItem(CURRENT_USER_NAME, String(obj.user_name));
      }

      function getUserId() {
        return localStorage.getItem(CURRENT_USER_ID);
      }

      function getUserName() {
        return localStorage.getItem(CURRENT_USER_NAME);
      } // Grid


      function setGridPageSize(val) {
        localStorage.setItem(GRID_PAGE_SIZE, String(val));
      }

      function getGridPageSize() {
        return localStorage.getItem(GRID_PAGE_SIZE);
      }
      /***/

    },

    /***/
    "./src/app/shared/functions/unsubscribe.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/functions/unsubscribe.ts ***!
      \*************************************************/

    /*! exports provided: unSubscribe */

    /***/
    function srcAppSharedFunctionsUnsubscribeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "unSubscribe", function () {
        return unSubscribe;
      });

      function unSubscribe(subs) {
        if (subs !== null && subs !== undefined) {
          subs.unsubscribe();
        }
      }
      /***/

    },

    /***/
    "./src/app/shared/pipes/sanitize-html.ts":
    /*!***********************************************!*\
      !*** ./src/app/shared/pipes/sanitize-html.ts ***!
      \***********************************************/

    /*! exports provided: SanitizeHtmlPipe */

    /***/
    function srcAppSharedPipesSanitizeHtmlTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SanitizeHtmlPipe", function () {
        return SanitizeHtmlPipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

      var SanitizeHtmlPipe = /*#__PURE__*/function () {
        function SanitizeHtmlPipe(sanitizer) {
          _classCallCheck(this, SanitizeHtmlPipe);

          this.sanitizer = sanitizer;
        }

        _createClass(SanitizeHtmlPipe, [{
          key: "transform",
          value: function transform(value) {
            return this.sanitizer.bypassSecurityTrustHtml(value);
          }
        }]);

        return SanitizeHtmlPipe;
      }();

      SanitizeHtmlPipe.ctorParameters = function () {
        return [{
          type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]
        }];
      };

      SanitizeHtmlPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'sanitizeHtml'
      })], SanitizeHtmlPipe);
      /***/
    },

    /***/
    "./src/app/shared/services/api.service.ts":
    /*!************************************************!*\
      !*** ./src/app/shared/services/api.service.ts ***!
      \************************************************/

    /*! exports provided: ApiService */

    /***/
    function srcAppSharedServicesApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ApiService", function () {
        return ApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _http_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./http-api.service */
      "./src/app/shared/services/http-api.service.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");

      var ApiService = /*#__PURE__*/function () {
        function ApiService(httpApi) {
          _classCallCheck(this, ApiService);

          this.httpApi = httpApi;
        } // api - инициализация приложения


        _createClass(ApiService, [{
          key: "getCSRFToken",
          value: function getCSRFToken() {
            return this.httpApi.get(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].initUrl);
          } // api - вход

        }, {
          key: "apiLogin",
          value: function apiLogin(params) {
            return this.httpApi.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].loginUrl, params);
          } // api - выход

        }, {
          key: "apiLogout",
          value: function apiLogout() {
            return this.httpApi.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].logoutUrl);
          }
        }, {
          key: "List",
          value: function List(url, params) {
            return this.httpApi.get(url, params);
          }
        }, {
          key: "Create",
          value: function Create(url, params) {
            return this.httpApi.post(url, params);
          }
        }, {
          key: "Update",
          value: function Update(url, params, id) {
            var fullUrl = url + id + '/';
            return this.httpApi.put(fullUrl, params);
          }
        }, {
          key: "Delete",
          value: function Delete(url, id) {
            var fullUrl = url + id + '/';
            return this.httpApi["delete"](fullUrl);
          }
        }]);

        return ApiService;
      }();

      ApiService.ctorParameters = function () {
        return [{
          type: _http_api_service__WEBPACK_IMPORTED_MODULE_2__["HttpApiService"]
        }];
      };

      ApiService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], ApiService);
      /***/
    },

    /***/
    "./src/app/shared/services/auth.guard.ts":
    /*!***********************************************!*\
      !*** ./src/app/shared/services/auth.guard.ts ***!
      \***********************************************/

    /*! exports provided: AuthGuard */

    /***/
    function srcAppSharedServicesAuthGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthGuard", function () {
        return AuthGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth.service */
      "./src/app/shared/services/auth.service.ts");

      var AuthGuard = /*#__PURE__*/function () {
        function AuthGuard(authService, router) {
          _classCallCheck(this, AuthGuard);

          this.authService = authService;
          this.router = router;
        }

        _createClass(AuthGuard, [{
          key: "canActivate",
          value: function canActivate(next, state) {
            if (this.authService.isLoggedIn) {
              return true;
            } // noinspection JSIgnoredPromiseFromCall


            this.router.navigateByUrl('/login');
            return false;
          } // Проверка доступа к разделам по ролям

        }, {
          key: "canActivateChild",
          value: function canActivateChild(childRoute, state) {
            if (this.authService.isLoggedIn) {
              return true;
            } // noinspection JSIgnoredPromiseFromCall


            this.router.navigateByUrl('/login');
            return false;
          }
        }]);

        return AuthGuard;
      }();

      AuthGuard.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], AuthGuard);
      /***/
    },

    /***/
    "./src/app/shared/services/auth.service.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/services/auth.service.ts ***!
      \*************************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppSharedServicesAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");

      var AuthService = /*#__PURE__*/function () {
        function AuthService() {
          _classCallCheck(this, AuthService);

          this.loggedIn = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](false);
        }

        _createClass(AuthService, [{
          key: "setLoggedIn",
          value: function setLoggedIn(val) {
            this.loggedIn.next(val);
          }
        }, {
          key: "clear",
          value: function clear() {
            this.loggedIn.next(false);
          }
        }, {
          key: "isLoggedIn",
          get: function get() {
            return this.loggedIn.getValue();
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], AuthService);
      /***/
    },

    /***/
    "./src/app/shared/services/csrf.service.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/services/csrf.service.ts ***!
      \*************************************************/

    /*! exports provided: HttpXsrfInterceptor */

    /***/
    function srcAppSharedServicesCsrfServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HttpXsrfInterceptor", function () {
        return HttpXsrfInterceptor;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var HttpXsrfInterceptor = /*#__PURE__*/function () {
        function HttpXsrfInterceptor(tokenExtractor) {
          _classCallCheck(this, HttpXsrfInterceptor);

          this.tokenExtractor = tokenExtractor;
        }

        _createClass(HttpXsrfInterceptor, [{
          key: "intercept",
          value: function intercept(req, next) {
            var headerName = 'x-csrftoken';
            var token = this.tokenExtractor.getToken();

            if (token !== null && !req.headers.has(headerName)) {
              req = req.clone({
                headers: req.headers.set(headerName, token)
              });
            }

            return next.handle(req);
          }
        }]);

        return HttpXsrfInterceptor;
      }();

      HttpXsrfInterceptor.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpXsrfTokenExtractor"]
        }];
      };

      HttpXsrfInterceptor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()], HttpXsrfInterceptor);
      /***/
    },

    /***/
    "./src/app/shared/services/http-api.service.ts":
    /*!*****************************************************!*\
      !*** ./src/app/shared/services/http-api.service.ts ***!
      \*****************************************************/

    /*! exports provided: HttpApiService */

    /***/
    function srcAppSharedServicesHttpApiServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HttpApiService", function () {
        return HttpApiService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var HttpApiService_1;

      var HttpApiService = HttpApiService_1 = /*#__PURE__*/function () {
        function HttpApiService(http) {
          _classCallCheck(this, HttpApiService);

          this.http = http;
        } // Сборка параметров для запроса


        _createClass(HttpApiService, [{
          key: "get",
          value: function get(url) {
            var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var httpParams = HttpApiService_1.buildHttpParams(params);
            return this.http.get(url, {
              params: httpParams,
              withCredentials: true
            });
          }
        }, {
          key: "post",
          value: function post(url) {
            var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var headers = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            return this.http.post(url, params, {
              headers: headers,
              withCredentials: true
            });
          }
        }, {
          key: "put",
          value: function put(url) {
            var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            var headers = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
            return this.http.put(url, params, {
              headers: headers,
              withCredentials: true
            });
          }
        }, {
          key: "delete",
          value: function _delete(url) {
            var headers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
            return this.http["delete"](url, {
              headers: headers,
              withCredentials: true
            });
          }
        }], [{
          key: "buildHttpParams",
          value: function buildHttpParams(params) {
            var httpParams = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]();

            for (var key in params) {
              if (params[key] || params[key] === '') {
                httpParams = httpParams.set(key, params[key]);
              }
            }

            return httpParams;
          }
        }]);

        return HttpApiService;
      }();

      HttpApiService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      HttpApiService = HttpApiService_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], HttpApiService);
      /***/
    },

    /***/
    "./src/app/shared/services/init.service.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/services/init.service.ts ***!
      \*************************************************/

    /*! exports provided: InitService */

    /***/
    function srcAppSharedServicesInitServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InitService", function () {
        return InitService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./api.service */
      "./src/app/shared/services/api.service.ts");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth.service */
      "./src/app/shared/services/auth.service.ts");
      /* harmony import */


      var _functions_localstorage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../functions/localstorage */
      "./src/app/shared/functions/localstorage.ts");

      var InitService = /*#__PURE__*/function () {
        function InitService(api, authService) {
          _classCallCheck(this, InitService);

          this.api = api;
          this.authService = authService;
        }

        _createClass(InitService, [{
          key: "init",
          value: function init() {
            var _this10 = this;

            // обнулить сторадж
            Object(_functions_localstorage__WEBPACK_IMPORTED_MODULE_4__["clearLocalStorage"])(); // получение куки с токеном

            var promise = this.api.getCSRFToken().toPromise();
            promise.then(function (resp) {
              Object(_functions_localstorage__WEBPACK_IMPORTED_MODULE_4__["setUserInfo"])(resp.result);

              _this10.authService.setLoggedIn(resp.result.auth);
            });
            return promise;
          }
        }]);

        return InitService;
      }();

      InitService.ctorParameters = function () {
        return [{
          type: _api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }];
      };

      InitService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], InitService);
      /***/
    },

    /***/
    "./src/app/shared/shared.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/shared/shared.module.ts ***!
      \*****************************************/

    /*! exports provided: SharedModule */

    /***/
    function srcAppSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
        return SharedModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _components_grid_paging_grid_paging_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./components/grid-paging/grid-paging.component */
      "./src/app/shared/components/grid-paging/grid-paging.component.ts");
      /* harmony import */


      var _clr_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @clr/angular */
      "./node_modules/@clr/angular/__ivy_ngcc__/fesm2015/clr-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _components_grid_base_grid_base_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./components/grid-base/grid-base.component */
      "./src/app/shared/components/grid-base/grid-base.component.ts");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./services/auth.service */
      "./src/app/shared/services/auth.service.ts");
      /* harmony import */


      var _services_auth_guard__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./services/auth.guard */
      "./src/app/shared/services/auth.guard.ts");
      /* harmony import */


      var _services_api_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./services/api.service */
      "./src/app/shared/services/api.service.ts");
      /* harmony import */


      var _services_http_api_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./services/http-api.service */
      "./src/app/shared/services/http-api.service.ts");
      /* harmony import */


      var _pipes_sanitize_html__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./pipes/sanitize-html */
      "./src/app/shared/pipes/sanitize-html.ts");

      var SharedModule_1;

      var SharedModule = SharedModule_1 = /*#__PURE__*/function () {
        function SharedModule() {
          _classCallCheck(this, SharedModule);
        }

        _createClass(SharedModule, null, [{
          key: "forRoot",
          value: function forRoot() {
            return {
              ngModule: SharedModule_1,
              providers: [_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _services_auth_guard__WEBPACK_IMPORTED_MODULE_8__["AuthGuard"], _services_api_service__WEBPACK_IMPORTED_MODULE_9__["ApiService"], _services_http_api_service__WEBPACK_IMPORTED_MODULE_10__["HttpApiService"]]
            };
          }
        }]);

        return SharedModule;
      }();

      SharedModule = SharedModule_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrIconModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrDatagridModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrCommonFormsModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrInputModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrSelectModule"], _clr_angular__WEBPACK_IMPORTED_MODULE_3__["ClrDatepickerModule"]],
        exports: [_components_grid_paging_grid_paging_component__WEBPACK_IMPORTED_MODULE_2__["GridPagingComponent"], _components_grid_base_grid_base_component__WEBPACK_IMPORTED_MODULE_6__["GridBaseComponent"]],
        declarations: [_components_grid_paging_grid_paging_component__WEBPACK_IMPORTED_MODULE_2__["GridPagingComponent"], _components_grid_base_grid_base_component__WEBPACK_IMPORTED_MODULE_6__["GridBaseComponent"], _pipes_sanitize_html__WEBPACK_IMPORTED_MODULE_11__["SanitizeHtmlPipe"]],
        providers: []
      })], SharedModule);
      /***/
    },

    /***/
    "./src/environments/environment.ts":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var HOST = '';
      var environment = {
        production: true,
        host: HOST,
        initUrl: HOST + '/api/init',
        loginUrl: HOST + '/api/auth/login',
        logoutUrl: HOST + '/api/auth/logout'
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "./src/main.ts":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function srcMainTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/platform-browser-dynamic */
      "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/platform-browser-dynamic.js");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "./src/app/app.module.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "./src/environments/environment.ts");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/
    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /home/and/Projects/MyOwn/storage_django_angular/frontend/src/main.ts */
      "./src/main.ts");
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map